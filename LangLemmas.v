From BiDir.Lemmas Require Import VarShiftUpDecidable.
From BiDir.Lemmas Require Import MonotypeDecidable.

Module Var.
  Module Shift.
    Module Up.
      Definition Decidable := VarShiftUpDecidable.
    End Up.
  End Shift.
End Var.

Module Monotype.
  Definition Decidable := MonotypeDecidable.
End Monotype.
