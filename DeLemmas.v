From BiDir.Lemmas Require Import
  DeCtxInVarDeTypeWF
  DeCtxCutBVarDeTypeWF
  ChkOfSyn
  SysFInfDeTypeWF
  SubRefl
  SubtypingCoercion
  DeSubDeTypeWF
  DeChkDeTypeWF
  UniMapDeTypeWF.

Module Ctx.
  Module TypeWF.
    Definition In := DeCtxInVarDeTypeWF.
  End TypeWF.
  Module Cut.
    Module BVar.
      Definition TypeWF := DeCtxCutBVarDeTypeWF.
    End BVar.
  End Cut.
End Ctx.

Module DeSub.
  Definition Refl := SubRefl.

  Definition TypeWF := DeSubDeTypeWF.
End DeSub.

Module DeChk.
  Definition TypeWF := DeChkDeTypeWF.

  Module Of.
    Definition DeSyn := ChkOfSyn.
  End Of.
End DeChk.

Module SysFInf.
  Definition TypeWF := SysFInfDeTypeWF.

  Module Id.
    Module Coercing.
      Definition OfDeSub := SubtypingCoercion.
    End Coercing.
  End Id.
End SysFInf.

Module UniMap.
  Definition TypeWF := UniMapTypeWF.
End UniMap.
