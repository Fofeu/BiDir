From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.
From BiDir Require Import SystemF.
From BiDir Require Import BetaEta.
From BiDir Require LangLemmas.
From BiDir Require DeLemmas.
From BiDir Require BELemmas.

From Coq Require Import Lia.
From Hammer Require Import Tactics Hammer.

Theorem DeCompleteness:
  forall ctx e t,
    SysFInf ctx e t -> exists e', DeSyn ctx e' t /\ Erasure e' e.
Proof.
  intros ctx e t HSysFInf.
  induction HSysFInf.
  - exists Unt; repeat (constructor || auto).
  - exists (Var v); repeat (constructor || auto).
  - destruct IHHSysFInf as [body' [HSysFInfbody HErasure]].
    assert (HECtxWF: DeCtxWF (BVar tl::ctx)) by (now inversion HSysFInf).
    assert (HCtxWF: DeCtxWF ctx) by now inversion HECtxWF.
    assert (HTypeWFtl: DeTypeWF ctx tl) by (now inversion HECtxWF).
    assert (HTypeWFtr: DeTypeWF (BVar tl :: ctx) tr) by (eapply DeLemmas.SysFInf.TypeWF; eauto).
    destruct (LangLemmas.Monotype.Decidable (TFun tl tr)) as [ HMonotype | HNMonotype ].
    + exists (Lam body').
      split; try now constructor.
      constructor; auto.
      * constructor; auto.
        apply (DeLemmas.Ctx.Cut.BVar.TypeWF (BVar tl :: ctx) _  HTypeWFtr 0).
        constructor.
      * apply DeLemmas.DeChk.Of.DeSyn; auto.
    + exists (Ant (Lam body') (TFun tl tr)).
      split; try (repeat constructor; now auto).
      constructor; auto.
      * constructor; auto.
        apply (DeLemmas.Ctx.Cut.BVar.TypeWF (BVar tl :: ctx) _ HTypeWFtr 0).
        constructor.
      * apply DeChkLam; auto.
        now apply DeLemmas.DeChk.Of.DeSyn.
  - destruct IHHSysFInf1 as [f' [HSynf' HErasuref]].
    destruct IHHSysFInf2 as [arg' [HSynarg' HErasurearg]].
    exists (App f' arg').
    split; try now constructor.
    apply (DeSynApp _ _ _ (TFun tl tr)); auto.
    constructor; auto.
    + assert (HProof: DeTypeWF ctx (TFun tl tr)) by now apply (DeLemmas.SysFInf.TypeWF ctx f (TFun tl tr)).
      now inversion HProof.
    + now apply DeLemmas.DeChk.Of.DeSyn.
  - destruct IHHSysFInf as [e' [HSyn HErasure]].
    exists (Ant e' (TAll t)).
    split; try now constructor.
    constructor; auto.
    constructor.
    apply (DeLemmas.SysFInf.TypeWF _ e); auto.
    constructor; auto.
    now apply DeLemmas.DeChk.Of.DeSyn.
  - destruct IHHSysFInf as [e' [HSyn HErasure]].
    exists (Ant e' t').
    split; try now constructor.
    assert (HTypeWFTAll: DeTypeWF ctx (TAll t)) by (apply (DeLemmas.SysFInf.TypeWF _ e); auto).
    assert (HTypeWFt': DeTypeWF ctx t') by (now apply (DeLemmas.UniMap.TypeWF ctx (TAll t) tau)).
    constructor; auto.
    apply (DeChkSub _ _ _ (TAll t)); auto.
    apply (DeSubTAlL ctx _ _ tau t'); auto.
    apply DeLemmas.DeSub.Refl; auto.
Qed.

Print Assumptions DeCompleteness.

Theorem DeSoundnessChk:
  forall ctx e t,
    DeChk ctx e t -> exists e' e'', SysFInf ctx e' t /\  Erasure e e'' /\ BEtaEq e' e''.
Proof.
  intros ctx e t HChk.
  induction HChk as
    [ ctx HCtxWF
    | ctx v t HCtxWF HCtxIn
    | ctx body tl tr HCtxWF HTypeWF HMonotype HChk IHHChk
    | ctx f arg tf tout HCtxWF HSynf IHHSynf HApp IHHApp
    | ctx e_annot annot HCtxWF HTypeWF HChk IHHChk
    | ctx HCtxWF
    | ctx body tl tr HCtxWF HChk IHHChk
    | ctx e_sub t t_syn HCtxWF HSyn IHHSyn HSub
    | ctx e_abs tabs HCtxWF HChk IHHChk
    | ctx tl tr arg HCtxWF HTypeWF HChk IHHChk
    | ctx tabs arg tout tau tabs' HCtxWF HTypeWFtau HMonotype HUniMap HApp IHHApp
    ]
      using dechk_mut with
      (P:=fun ctx e t HSyn => exists e' e'', SysFInf ctx e' t /\ Erasure e e'' /\ BEtaEq e' e'')
      (P1:=fun ctx tf arg tout HApp => forall f', SysFInf ctx f' tf -> exists arg' arg'', SysFInf ctx (App f' arg') tout /\ Erasure arg arg'' /\ BEtaEq arg' arg'')
  .
  - exists Unt, Unt; repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - exists (Var v), (Var v); repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - destruct IHHChk as [ body' [ body'' [ HSysFbody [ HErasurebody HBEtaEqbody ] ] ] ].
    exists (Lam body'), (Lam body'').
    repeat split.
    + now constructor.
    + now constructor.
    + inversion HBEtaEqbody; subst.
      apply (BEEq _ _ (Lam e) n m); now apply (BELemmas.BEtas.Lam).
  - destruct IHHSynf as [ f' [ f'' [ HSysFf' [ HErasuref HBEtaEq ] ] ] ].
    destruct (IHHApp f' HSysFf') as [ arg' [ arg'' [ HSysFApp [ HErasurearg HBEtaEqarg ] ] ] ].
    inversion HBEtaEq; subst.
    inversion HBEtaEqarg; subst.
    exists (App f' arg'), (App f'' arg'').
    repeat split.
    + inversion HSysFApp; subst.
      * apply (SysFApp ctx f' arg' tl tout); auto.
      * apply (SysFAbI ctx (App f' arg') t); auto.
      * apply (SysFAbE ctx (App f' arg') t tau); auto.
    + now constructor.
    + econstructor; apply BELemmas.BEtas.Chain.App; eauto.
  - destruct IHHChk as [ e' [ e'' [ HSysFInf [ HErasure HBEtaEq ] ] ] ].
    exists e', e''.
    repeat split; try constructor; auto.
  - exists Unt, Unt; repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - destruct IHHChk as [ body' [ body'' [ HSysFInfbody' [ HErasurebody HBEtaEq ] ] ] ].
    inversion HBEtaEq; subst.
    exists (Lam body'), (Lam body'').
    repeat split.
    + now constructor.
    + now constructor.
    + econstructor; apply BELemmas.BEtas.Lam; eauto.
  - destruct IHHSyn as [ e_sub' [ e_sub'' [ HSysF [ HErasure HBEtaEq ] ] ] ] .
    destruct (DeLemmas.SysFInf.Id.Coercing.OfDeSub ctx t_syn t HSub) as [ id [ HSysFid HBEtaEqid ] ].
    destruct HBEtaEq as [ e_sub' e_sub'' e_sub''' nsub msub HBEtase_sub' HBEtase_sub'' ].
    remember (Lam (Var 0)) as lamvar0.
    destruct HBEtaEqid as [ id lamvar0 id' nid mid HBEtasid' HBEtasid ]; subst.
    destruct (BELemmas.BEtas.Id.BEta0 mid id' HBEtasid) as [ Heqn Heqe ]; subst.
    exists (App id e_sub'), e_sub''.
    repeat split.
    + apply (SysFApp _ _ _ t_syn); auto.
    + auto.
    + econstructor.
      apply (BELemmas.BEtas.Id.App _ _ _ HBEtase_sub' id nid HBEtasid').
      eauto.
  - destruct IHHChk as [ e_abs' [ e_abs'' [ HSysF [ HErasure HBEtaEq ] ] ] ].
    exists e_abs', e_abs''.
    repeat split; auto.
    constructor; auto.
  - destruct IHHChk as [ arg' [ arg'' [ HSysFarg' [ HErasure HBEtaEq ] ] ] ].
    intros f' HSysFf'.
    exists arg', arg''.
    repeat split; auto.
    apply (SysFApp ctx f' arg' tl tr); auto.
  - intros f' HSysFf'.
    assert (HSysFf'2: SysFInf ctx f' tabs') by
      (apply (SysFAbE ctx f' tabs tau tabs'); auto).
    destruct (IHHApp f' HSysFf'2) as [ arg' [ arg'' [ HSysFApp [ HErasure HBEtaEq ] ] ] ].
    exists arg', arg''.
    repeat split; auto.
Qed.

Print Assumptions DeSoundnessChk.

Theorem DeSoundnessSyn:
  forall ctx e t,
    DeSyn ctx e t  -> exists e' e'', SysFInf ctx e' t /\  Erasure e e'' /\ BEtaEq e' e''.
Proof.
  intros ctx e t HSyn.
  induction HSyn
    as
    [ ctx HCtxWF
    | ctx v t HCtxWF HCtxIn
    | ctx body tl tr HCtxWF HTypeWF HMonotype HChk IHHChk
    | ctx f arg tf tout HCtxWF HSynf IHHSynf HApp IHHApp
    | ctx e_annot annot HCtxWF HTypeWF HChk IHHChk
    | ctx HCtxWF
    | ctx body tl tr HCtxWF HChk IHHChk
    | ctx e_sub t t_syn HCtxWF HSyn IHHSyn HSub
    | ctx e_abs tabs HCtxWF HChk IHHChk
    | ctx tl tr arg HCtxWF HTypeWF HChk IHHChk
    | ctx tabs arg tout tau tabs' HCtxWF HTypeWFtau HMonotype HUniMap HApp IHHApp
    ]
    using desyn_mut with
      (P0:=fun ctx e t HChk => exists e' e'', SysFInf ctx e' t /\ Erasure e e'' /\ BEtaEq e' e'')
      (P1:=fun ctx tf arg tout HApp => forall f', SysFInf ctx f' tf -> exists arg' arg'', SysFInf ctx (App f' arg') tout /\ Erasure arg arg'' /\ BEtaEq arg' arg'')
  .
  - exists Unt, Unt; repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - exists (Var v), (Var v); repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - destruct IHHChk as [ body' [ body'' [ HSysFbody [ HErasurebody HBEtaEqbody ] ] ] ].
    exists (Lam body'), (Lam body'').
    repeat split.
    + now constructor.
    + now constructor.
    + inversion HBEtaEqbody; subst.
      apply (BEEq _ _ (Lam e) n m); now apply (BELemmas.BEtas.Lam).
  - destruct IHHSynf as [ f' [ f'' [ HSysFf' [ HErasuref HBEtaEq ] ] ] ].
    destruct (IHHApp f' HSysFf') as [ arg' [ arg'' [ HSysFApp [ HErasurearg HBEtaEqarg ] ] ] ].
    inversion HBEtaEq; subst.
    inversion HBEtaEqarg; subst.
    exists (App f' arg'), (App f'' arg'').
    repeat split.
    + inversion HSysFApp; subst.
      * apply (SysFApp ctx f' arg' tl tout); auto.
      * apply (SysFAbI ctx (App f' arg') t); auto.
      * apply (SysFAbE ctx (App f' arg') t tau); auto.
    + now constructor.
    + econstructor; apply BELemmas.BEtas.Chain.App; eauto.
  - destruct IHHChk as [ e' [ e'' [ HSysFInf [ HErasure HBEtaEq ] ] ] ].
    exists e', e''.
    repeat split; try constructor; auto.
  - exists Unt, Unt; repeat constructor; auto; apply BELemmas.BEtaEq.Refl.
  - destruct IHHChk as [ body' [ body'' [ HSysFInfbody' [ HErasurebody HBEtaEq ] ] ] ].
    inversion HBEtaEq; subst.
    exists (Lam body'), (Lam body'').
    repeat split.
    + now constructor.
    + now constructor.
    + econstructor; apply BELemmas.BEtas.Lam; eauto.
  - destruct IHHSyn as [ e_sub' [ e_sub'' [ HSysF [ HErasure HBEtaEq ] ] ] ] .
    destruct (DeLemmas.SysFInf.Id.Coercing.OfDeSub ctx t_syn t HSub) as [ id [ HSysFid HBEtaEqid ] ].
    destruct HBEtaEq as [ e_sub' e_sub'' e_sub''' nsub msub HBEtase_sub' HBEtase_sub'' ].
    remember (Lam (Var 0)) as lamvar0.
    destruct HBEtaEqid as [ id lamvar0 id' nid mid HBEtasid' HBEtasid ]; subst.
    destruct (BELemmas.BEtas.Id.BEta0 mid id' HBEtasid) as [ Heqn Heqe ]; subst.
    exists (App id e_sub'), e_sub''.
    repeat split.
    + apply (SysFApp _ _ _ t_syn); auto.
    + auto.
    + econstructor.
      apply (BELemmas.BEtas.Id.App _ _ _ HBEtase_sub' id nid HBEtasid').
      eauto.
  - destruct IHHChk as [ e_abs' [ e_abs'' [ HSysF [ HErasure HBEtaEq ] ] ] ].
    exists e_abs', e_abs''.
    repeat split; auto.
    constructor; auto.
  - destruct IHHChk as [ arg' [ arg'' [ HSysFarg' [ HErasure HBEtaEq ] ] ] ].
    intros f' HSysFf'.
    exists arg', arg''.
    repeat split; auto.
    apply (SysFApp ctx f' arg' tl tr); auto.
  - intros f' HSysFf'.
    assert (HSysFf'2: SysFInf ctx f' tabs') by
      (apply (SysFAbE ctx f' tabs tau tabs'); auto).
    destruct (IHHApp f' HSysFf'2) as [ arg' [ arg'' [ HSysFApp [ HErasure HBEtaEq ] ] ] ].
    exists arg', arg''.
    repeat split; auto.
Qed.

Print Assumptions DeSoundnessChk.
