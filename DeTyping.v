From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Inductive DeSub: ctx -> type -> type -> Prop :=
| DeSubTUnt: forall ctx, DeCtxWF ctx -> DeSub ctx TUnt TUnt
| DeSubTFun: forall ctx tl1 tr1 tl2 tr2, DeCtxWF ctx -> DeSub ctx tl2 tl1 -> DeSub ctx tr1 tr2 -> DeSub ctx (TFun tl1 tr1) (TFun tl2 tr2)
| DeSubTUni: forall ctx n, DeCtxWF ctx -> DeCtxMemUni ctx n -> DeSub ctx (TUni n) (TUni n)
| DeSubTAlL: forall ctx tabs tr tau tabs', DeCtxWF ctx -> DeTypeWF ctx tau -> Monotype tau -> UniMap (TAll tabs) tau tabs' -> DeSub ctx tabs' tr -> DeSub ctx (TAll tabs) tr
| DeSubTAlR: forall ctx tl tr tl', DeCtxWF ctx -> UniShiftUp tl 0 tl' -> DeSub (BUni::ctx) tl' tr -> DeSub ctx tl (TAll tr)

with DeSyn: ctx -> expr -> type -> Prop :=
| DeSynUnt: forall ctx, DeCtxWF ctx -> DeSyn ctx Unt TUnt
| DeSynVar: forall ctx v t, DeCtxWF ctx -> DeCtxInVar ctx v t -> DeSyn ctx (Var v) t
| DeSynLam: forall ctx e tl tr, DeCtxWF ctx -> DeTypeWF ctx (TFun tl tr) -> Monotype (TFun tl tr) -> DeChk (BVar tl::ctx) e tr -> DeSyn ctx (Lam e) (TFun tl tr)
| DeSynApp: forall ctx f arg tf tapp, DeCtxWF ctx -> DeSyn ctx f tf -> DeApp ctx tf arg tapp -> DeSyn ctx (App f arg) tapp
| DeSynAnt: forall ctx e annot, DeCtxWF ctx -> DeTypeWF ctx annot -> DeChk ctx e annot -> DeSyn ctx (Ant e annot) annot

with DeChk: ctx -> expr -> type -> Prop :=
| DeChkUnt: forall ctx, DeCtxWF ctx -> DeChk ctx Unt TUnt
| DeChkLam: forall ctx body tl tr, DeCtxWF ctx -> DeChk (BVar tl::ctx) body tr -> DeChk ctx (Lam body) (TFun tl tr)
| DeChkSub: forall ctx e t tsyn, DeCtxWF ctx -> DeSyn ctx e tsyn -> DeSub ctx tsyn t -> DeChk ctx e t
| DeChkAbs: forall ctx e tabs, DeCtxWF ctx -> DeChk (BUni::ctx) e tabs -> DeChk ctx e (TAll tabs)

with DeApp: ctx -> type -> expr -> type -> Prop :=
| DeAppFun: forall ctx tl tr arg, DeCtxWF ctx -> DeTypeWF ctx tr -> DeChk ctx arg tl -> DeApp ctx (TFun tl tr) arg tr
| DeAppAbs: forall ctx tabs arg t tau tabs', DeCtxWF ctx -> DeTypeWF ctx tau -> Monotype tau -> UniMap (TAll tabs) tau tabs' -> DeApp ctx tabs' arg t -> DeApp ctx (TAll tabs) arg t
.

Scheme desyn_mut := Induction for DeSyn Sort Prop
with dechk_mut := Induction for DeChk Sort Prop
with deapp_mut := Induction for DeApp Sort Prop
.
