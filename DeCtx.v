From BiDir Require Import Lang.
From Coq.Lists Require Export List.
Export ListNotations.

Inductive bkind :=
| KVar
| KUni
| KExV
| KMrk
.

Inductive binding :=
| BVar: type -> binding
| BUni: binding
| BExV: binding
| BExE: type -> binding
| BMrk: binding
.

Definition binding_kind b :=
  match b with
  | BVar _ => KVar
  | BUni   => KUni
  | BExV   => KExV
  | BExE _ => KExV
  | BMrk   => KMrk
  end
.

Definition ctx := list binding.

Inductive DeCtxInVar: ctx -> nat -> type -> Prop :=
| DeCtxInVarBase: forall ctx t, DeCtxInVar (BVar t :: ctx) 0 t
| DeCtxInVarBVar: forall ctx v t t', DeCtxInVar ctx v t -> DeCtxInVar (BVar t' :: ctx) (S v) t
| DeCtxInVarBUni: forall ctx v t t', DeCtxInVar ctx v t -> UniShiftUp t 0 t' -> DeCtxInVar (BUni :: ctx) v t'
.

Inductive DeCtxMemVar: ctx -> nat -> Prop :=
  DeCtxMemVarBase: forall ctx v t, DeCtxInVar ctx v t -> DeCtxMemVar ctx v.

Inductive AlCtxInVar: ctx -> nat -> type -> Prop :=
| AlCtxInVarBase: forall ctx t, AlCtxInVar (BVar t :: ctx) 0 t
| AlCtxInVarBVar: forall ctx v t t', AlCtxInVar ctx v t -> AlCtxInVar (BVar t' :: ctx) (S v) t
| AlCtxInVarBUni: forall ctx v t t', AlCtxInVar ctx v t -> UniShiftUp t 0 t' -> AlCtxInVar (BUni :: ctx) v t'
| AlCtxInVarBExV: forall ctx v t t', AlCtxInVar ctx v t -> ExVShiftUp t 0 t' -> AlCtxInVar (BExV :: ctx) v t'
| AlCtxInVarBExE: forall ctx v t t' t'', AlCtxInVar ctx v t -> ExVShiftUp t 0 t' -> AlCtxInVar (BExE t'' :: ctx) v t'
| AlCtxInVarBMrk: forall ctx v t, AlCtxInVar ctx v t -> AlCtxInVar (BMrk :: ctx) v t
.

Inductive AlCtxMemVar: ctx -> nat -> Prop :=
  AlCtxMemVarBase: forall ctx v t, AlCtxInVar ctx v t -> AlCtxMemVar ctx v.

Inductive DeCtxMemUni: ctx -> nat -> Prop :=
| DeCtxInUniBase: forall ctx, DeCtxMemUni (BUni :: ctx) 0
| DeCtxInUniBVar: forall ctx v t, DeCtxMemUni ctx v -> DeCtxMemUni (BVar t :: ctx) v
| DeCtxInUniBUni: forall ctx v, DeCtxMemUni ctx v -> DeCtxMemUni (BUni :: ctx) (S v)
.

Definition DeCtxInUni := DeCtxMemUni.

Inductive AlCtxMemUni: ctx -> nat -> Prop :=
| AlCtxMemUniBase: forall ctx, AlCtxMemUni (BUni :: ctx) 0
| AlCtxMemUniBVar: forall ctx v t, AlCtxMemUni ctx v -> AlCtxMemUni (BVar t :: ctx) v
| AlCtxMemUniBExV: forall ctx v, AlCtxMemUni ctx v -> AlCtxMemUni (BExV :: ctx) v
| AlCtxMemUniBExE: forall ctx v t, AlCtxMemUni ctx v -> AlCtxMemUni (BExE t :: ctx) v
| AlCtxMemUniBMrk: forall ctx v, AlCtxMemUni ctx v -> AlCtxMemUni (BMrk :: ctx) v
.

Definition AlCtxInUni := AlCtxMemUni.

Inductive AlCtxInExV: ctx -> nat -> type -> Prop :=
| AlCtxInExVBas1: forall ctx, AlCtxInExV (BExV :: ctx) 0 (TExV 0)
| AlCtxInExVBas2: forall ctx t, AlCtxInExV (BExE t :: ctx) 0 t
| AlCtxInExVBVar: forall ctx v t t', AlCtxInExV ctx v t -> AlCtxInExV (BVar t' :: ctx) v t
| AlCtxInExVBUni: forall ctx v t t', AlCtxInExV ctx v t -> UniShiftUp t 0 t' -> AlCtxInExV (BUni :: ctx) v t'
| AlCtxInExVBExV: forall ctx v t t', AlCtxInExV ctx v t -> ExVShiftUp t 0 t' -> AlCtxInExV (BExV :: ctx) v t'
| AlCtxInExVBExE: forall ctx v t t' t'', AlCtxInExV ctx v t -> ExVShiftUp t 0 t' -> AlCtxInExV (BExE t'' :: ctx) v t'
| AlCtxInExVBMrk: forall ctx v t, AlCtxInExV ctx v t -> AlCtxInExV (BMrk :: ctx) v t
.

Inductive AlCtxMemExV: ctx -> nat -> Prop :=
  AlCtxMemExVBase: forall ctx v t, AlCtxInExV ctx v t -> AlCtxMemExV ctx v.

Inductive DeTypeWF: ctx -> type -> Prop :=
| DeTypeWFTUnt: forall ctx, DeTypeWF ctx TUnt
| DeTypeWFTFun: forall ctx tl tr, DeTypeWF ctx tl -> DeTypeWF ctx tr -> DeTypeWF ctx (TFun tl tr)
| DeTypeWFTUni: forall ctx v, DeCtxMemUni ctx v -> DeTypeWF ctx (TUni v)
| DeTypeWFTAll: forall ctx tabs, DeTypeWF (BUni::ctx) tabs -> DeTypeWF ctx (TAll tabs)
.

Inductive AlTypeWF: ctx -> type -> Prop :=
| AlTypeWFTunt: forall ctx, AlTypeWF ctx TUnt
| AlTypeWFTFun: forall ctx tl tr, AlTypeWF ctx tl -> AlTypeWF ctx tr -> AlTypeWF ctx (TFun tl tr)
| AlTypeWFTUni: forall ctx v, AlCtxMemUni ctx v -> AlTypeWF ctx (TUni v)
| AlTypeWFTAll: forall ctx tabs, AlTypeWF (BUni::ctx) tabs -> AlTypeWF ctx (TAll tabs)
| AlTypeWFTExV: forall ctx v, AlCtxMemExV ctx v -> AlTypeWF ctx (TExV v)
.

Inductive DeCtxWF: ctx -> Prop :=
| DeCtxWFNil: DeCtxWF []
| DeCtxWFVar: forall ctx t, DeCtxWF ctx -> DeTypeWF ctx t -> DeCtxWF (BVar t :: ctx)
| DeCtxWFUni: forall ctx, DeCtxWF ctx -> DeCtxWF (BUni :: ctx)
.

Inductive AlCtxWF: ctx -> Prop :=
| AlCtxWFNil: AlCtxWF []
| AlCtxWFVar: forall ctx t, AlCtxWF ctx -> AlTypeWF ctx t -> AlCtxWF (BVar t :: ctx)
| AlCtxWFUni: forall ctx, AlCtxWF ctx -> AlCtxWF (BUni :: ctx)
| AlCtxWFExV: forall ctx, AlCtxWF ctx -> AlCtxWF (BExV :: ctx)
| AlCtxWFExE: forall ctx t, AlCtxWF ctx -> AlTypeWF ctx t -> AlCtxWF (BExE t :: ctx)
| AlCtxWFMrk: forall ctx, AlCtxWF ctx -> AlCtxWF (BMrk :: ctx)
.

Inductive DeCtxCutBVar: ctx -> nat -> ctx -> Prop :=
| DeCtxCutBVarBase: forall ctx tv, DeCtxCutBVar (BVar tv :: ctx) 0 ctx
| DeCtxCutBVarBVar: forall ctx tv n ctx', DeCtxCutBVar ctx n ctx' -> DeCtxCutBVar (BVar tv :: ctx) (S n) (BVar tv :: ctx')
| DeCtxCutBVarBUni: forall ctx n ctx', DeCtxCutBVar ctx n ctx' -> DeCtxCutBVar (BUni :: ctx) n (BUni :: ctx')
.

Inductive DeCtxInsertBVar: ctx -> nat -> type -> ctx -> Prop :=
| DeCtxInsertBVarBase: forall ctx t, DeCtxInsertBVar ctx 0 t (BVar t :: ctx)
| DeCtxInsertBVarBVar: forall ctx n t ctx' tv, DeCtxInsertBVar ctx n t ctx' -> DeCtxInsertBVar (BVar tv :: ctx) (S n) t (BVar tv :: ctx')
| DeCtxInsertBVarBUni: forall ctx n t ctx', DeCtxInsertBVar ctx n t ctx' -> DeCtxInsertBVar (BUni :: ctx) n t (BUni :: ctx')
.

Inductive DeCtxInsertBUni: ctx -> nat -> ctx -> Prop :=
| DeCtxInsertBUniBase: forall ctx, DeCtxInsertBUni ctx 0 (BUni :: ctx)
| DeCtxInsertBUniBVar: forall ctx n ctx' tv, DeCtxInsertBUni ctx n ctx' -> DeCtxInsertBUni (BVar tv :: ctx) n (BVar tv :: ctx')
| DeCtxInsertBUniBUni: forall ctx n ctx', DeCtxInsertBUni ctx n ctx' -> DeCtxInsertBUni (BUni :: ctx) (S n) (BUni :: ctx')
.

Inductive DeCtxCutBUni: ctx -> nat -> ctx -> Prop :=
| DeCtxCutBUniBase: forall ctx, DeCtxCutBUni (BUni :: ctx) 0 ctx
| DeCtxCutBUniBVar: forall ctx tv n ctx', DeCtxCutBUni ctx n ctx' -> DeCtxCutBUni (BVar tv :: ctx) n (BVar tv :: ctx')
| DeCtxCutBUniBUni: forall ctx n ctx', DeCtxCutBUni ctx n ctx' -> DeCtxCutBUni (BUni :: ctx) (S n) (BUni :: ctx')
.

Inductive DeCtxNoBVar: ctx -> Prop :=
| NoBVarBase: DeCtxNoBVar []
| NoBVarCons: forall ctx, DeCtxNoBVar ctx -> DeCtxNoBVar (BUni::ctx)
.
