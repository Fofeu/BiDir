Inductive type: Set :=
| TUnt: type
| TFun: type -> type -> type
| TUni: nat -> type
| TAll: type -> type
| TExV: nat -> type
.

Inductive expr: Set :=
| Unt: expr
| Var: nat -> expr
| Lam: expr -> expr
| App: expr -> expr -> expr
| Ant: expr -> type -> expr
.

Inductive Erasure: expr -> expr -> Prop :=
| ErasureUnt: Erasure Unt Unt
| ErasureVar: forall v, Erasure (Var v) (Var v)
| ErasureLam: forall body body', Erasure body body' -> Erasure (Lam body) (Lam body')
| ErasureApp: forall f arg f' arg', Erasure f f' -> Erasure arg arg' -> Erasure (App f arg) (App f' arg')
| ErasureAnt: forall e t e', Erasure e e' -> Erasure (Ant e t) e'
.

Inductive Monotype: type -> Prop :=
| MonoTUnt: Monotype TUnt
| MonoTFun: forall tl tr, Monotype tl -> Monotype tr -> Monotype (TFun tl tr)
| MonoTUni: forall v, Monotype (TUni v)
| MonoTExV: forall v, Monotype (TExV v)
.

Inductive MonotypeRoot: type -> type -> Prop :=
| MonoRootTUnt: MonotypeRoot TUnt TUnt
| MonoRootTFun: forall tl tr, MonotypeRoot (TFun tl tr) (TFun tl tr)
| MonoRootTUni: forall v, MonotypeRoot (TUni v) (TUni v)
| MonoRootTExV: forall v, MonotypeRoot (TExV v) (TExV v)
| MonoRootCons: forall t t', MonotypeRoot t t' -> MonotypeRoot (TAll t) t'
.

Inductive Value : expr -> Prop :=
| ValUnt: Value Unt
| ValLam: forall body, Value (Lam body)
.

Inductive UniShiftUp: type -> nat -> type -> Prop :=
| UniShUpTUnt: forall threshold, UniShiftUp TUnt threshold TUnt
| UniShUpTFun: forall tl tr threshold tl' tr', UniShiftUp tl threshold tl' -> UniShiftUp tr threshold tr' -> UniShiftUp (TFun tl tr) threshold (TFun tl' tr')
| UniShUpTUnM: forall v threshold, threshold <= v -> UniShiftUp (TUni v) threshold (TUni (S v))
| UniShUpTUnN: forall v threshold, v < threshold -> UniShiftUp (TUni v) threshold (TUni v)
| UniShUpTAll: forall tabs threshold tabs', UniShiftUp tabs (S threshold) tabs' -> UniShiftUp (TAll tabs) threshold (TAll tabs')
| UniShUpTExV: forall v threshold, UniShiftUp (TExV v) threshold (TExV v)
.

Inductive UniShiftDoSafe: type -> nat -> Prop :=
| UniShDoSafeTUnt: forall threshold, UniShiftDoSafe TUnt threshold
| UniShDoSafeTFun: forall tl tr threshold, UniShiftDoSafe tl threshold -> UniShiftDoSafe tr threshold -> UniShiftDoSafe (TFun tl tr) threshold
| UniShDoSafeTUn1: forall threshold, UniShiftDoSafe (TUni 0) (S threshold)
| UniShDoSafeTUn2: forall v threshold, UniShiftDoSafe (TUni (S v)) threshold
| UniShDoSafeTAll: forall tabs threshold, UniShiftDoSafe tabs (S threshold) -> UniShiftDoSafe (TAll tabs) threshold
| UniShDoSafeTExV: forall v threshold, UniShiftDoSafe (TExV v) threshold
.

Inductive UniShiftDo: type -> nat -> type -> Prop :=
| UniShDoTUnt: forall v, UniShiftDo TUnt v TUnt
| UniShDoTFun: forall tl tr threshold tl' tr', UniShiftDo tl threshold tl' -> UniShiftDo tr threshold tr' -> UniShiftDo (TFun tl tr) threshold (TFun tl' tr')
| UniShDoTUnM: forall v threshold, threshold <= (S v) -> UniShiftDo (TUni (S v)) threshold (TUni v)
| UniShDoTUnN: forall v threshold, v < threshold -> UniShiftDo (TUni v) threshold (TUni v)
| UniShDoTAll: forall tabs threshold tabs', UniShiftDo tabs (S threshold) tabs' -> UniShiftDo (TAll tabs) threshold (TAll tabs')
| UniShDoTExV: forall v threshold, UniShiftDo (TExV v) threshold (TExV v)
.

Inductive UniSubst: type -> nat -> type -> type -> Prop :=
| UniSubstTUnt: forall threshold sub, UniSubst TUnt threshold sub TUnt
| UniSubstTFun: forall tl tr threshold sub tl' tr', UniSubst tl threshold sub tl' -> UniSubst tr threshold sub tr' -> UniSubst (TFun tl tr) threshold sub (TFun tl' tr')
| UniSubstTUEq: forall v sub, UniSubst (TUni v) v sub sub
| UniSubstTUNE: forall v threshold sub, threshold <> v -> UniSubst (TUni v) threshold sub (TUni v)
| UniSubstTAll: forall t threshold sub sub' t', UniShiftUp sub 0 sub' -> UniSubst t (S threshold) sub' t' -> UniSubst (TAll t) threshold sub (TAll t')
| UniSubstTExV: forall v threshold sub, UniSubst (TExV v) threshold sub (TExV v)
.

Inductive UniMap: type -> type -> type -> Prop :=
| UniMapBase: forall tabs sub tout sub' tabs', UniShiftUp sub 0 sub' -> UniSubst tabs 0 sub' tabs' -> UniShiftDo tabs' 0 tout -> UniMap (TAll tabs) sub tout
.

Inductive ExVShiftUp: type -> nat -> type -> Prop :=
| ExVShUpTUnt: forall threshold, ExVShiftUp TUnt threshold TUnt
| ExVShUpTFun: forall tl tr threshold tl' tr', ExVShiftUp tl threshold tl' -> ExVShiftUp tr threshold tr' -> ExVShiftUp (TFun tl tr) threshold (TFun tl' tr')
| ExVShUpTUni: forall v threshold, ExVShiftUp (TUni v) threshold (TUni v)
| ExVShUpTAll: forall tabs threshold tabs', ExVShiftUp tabs threshold tabs' -> ExVShiftUp (TAll tabs) threshold tabs'
| ExVShUpTExM: forall v threshold, threshold <= v -> ExVShiftUp (TExV v) threshold (TExV (S v))
| ExVShUpTExN: forall v threshold, v < threshold -> ExVShiftUp (TExV v) threshold (TExV (S v))
.

Inductive ExVShiftDoSafe: type -> nat -> Prop :=
| ExVShDoSafeTUnt: forall threshold, ExVShiftDoSafe TUnt threshold
| ExVShDoSafeTFun: forall tl tr threshold, ExVShiftDoSafe tl threshold -> ExVShiftDoSafe tr threshold -> ExVShiftDoSafe (TFun tl tr) threshold
| ExVShDoSafeTUni: forall v threshold, ExVShiftDoSafe (TUni v) threshold
| ExVShDoSafeTAll: forall tabs threshold, ExVShiftDoSafe tabs (S threshold) -> ExVShiftDoSafe (TAll tabs) threshold
| ExVShDoSafeTEx1: forall threshold, ExVShiftDoSafe (TExV 0) (S threshold)
| ExVShDoSafeTEx2: forall v threshold, ExVShiftDoSafe (TExV (S v)) threshold
.

Inductive ExVSubst: type -> nat -> type -> type -> Prop :=
| ExVSubstTUnt: forall threshold sub, ExVSubst TUnt threshold sub TUnt
| ExVSubstTFun: forall tl tr threshold sub tl' tr', ExVSubst tl threshold sub tl' -> ExVSubst tr threshold sub tr' -> ExVSubst (TFun tl tr) threshold sub (TFun tl' tr')
| ExVSubstTUni: forall v threshold sub, ExVSubst (TExV v) threshold sub (TExV v)
| ExVSubstTAll: forall t threshold sub sub' t', ExVShiftUp sub 0 sub' -> ExVSubst t (S threshold) sub' t' -> ExVSubst (TAll t) threshold sub (TAll t')
| ExVSubstTEEq: forall v sub, ExVSubst (TExV v) v sub sub
| ExVSubstTENE: forall v threshold sub, threshold <> v -> ExVSubst (TExV v) threshold sub (TExV v)
.

Inductive VarShiftUp: expr -> nat -> expr -> Prop :=
| VarShUpUnt: forall threshold, VarShiftUp Unt threshold Unt
| VarShUpVaM: forall v threshold, threshold <= v -> VarShiftUp (Var v) threshold (Var (S v))
| VarShUpVaN: forall v threshold, v < threshold -> VarShiftUp (Var v) threshold (Var v)
| VarShUpLam: forall body threshold body', VarShiftUp body (S threshold) body' -> VarShiftUp (Lam body) threshold (Lam body')
| VarShUpApp: forall f arg threshold f' arg', VarShiftUp f threshold f' -> VarShiftUp arg threshold arg' -> VarShiftUp (App f arg) threshold (App f' arg')
| VarShUpAnt: forall e annot threshold e', VarShiftUp e threshold e' -> VarShiftUp (Ant e annot) threshold (Ant e' annot)
.

Inductive VarShiftDo: expr -> nat -> expr -> Prop :=
| VarShDoUnt: forall threshold, VarShiftDo Unt threshold Unt
| VarShDoVaM: forall v threshold, threshold <= (S v) -> VarShiftDo (Var (S v)) threshold (Var v)
| VarShDoVaN: forall v threshold, v < threshold -> VarShiftDo (Var v) threshold (Var v)
| VarShDoLam: forall body threshold body', VarShiftDo body (S threshold) body' -> VarShiftDo (Lam body) threshold (Lam body')
| VarShDoApp: forall f arg threshold f' arg', VarShiftDo f threshold f' -> VarShiftDo arg threshold arg' -> VarShiftDo (App f arg) threshold (App f' arg')
| VarShDoAnt: forall e annot threshold e', VarShiftDo e threshold e' -> VarShiftDo (Ant e annot) threshold (Ant e' annot)
.

Inductive VarShiftDoSafe: expr -> nat -> Prop :=
| VarShDoSafeUnt: forall n, VarShiftDoSafe Unt n
| VarShDoSafeVa1: forall n, VarShiftDoSafe (Var 0) (S n)
| VarShDoSafeVa2: forall v n, VarShiftDoSafe (Var (S v)) n
| VarShDoSafeLam: forall body n, VarShiftDoSafe body (S n) -> VarShiftDoSafe (Lam body) n
| VarShDoSafeApp: forall f arg n, VarShiftDoSafe f n -> VarShiftDoSafe arg n -> VarShiftDoSafe (App f arg) n
| VarShDoSafeAnt: forall e annot n, VarShiftDoSafe e n -> VarShiftDoSafe (Ant e annot) n
.

Inductive VarSubst: expr -> nat -> expr -> expr -> Prop :=
| VarSubstUnt: forall threshold sub, VarSubst Unt threshold sub Unt
| VarSubstVaE: forall v sub, VarSubst (Var v) v sub sub
| VarSubstVaN: forall v threshold sub, threshold <> v -> VarSubst (Var v) threshold sub (Var v)
| VarSubstLam: forall body threshold sub sub' body', VarShiftUp sub 0 sub' -> VarSubst body (S threshold) sub' body' -> VarSubst (Lam body) threshold sub (Lam body')
| VarSubstApp: forall f arg threshold sub f' arg', VarSubst f threshold sub f' -> VarSubst arg threshold sub arg' -> VarSubst (App f arg) threshold sub (App f' arg')
| VarSubstAnt: forall e annot threshold sub e', VarSubst e threshold sub e' -> VarSubst (Ant e annot) threshold sub (Ant e' annot)
.

Inductive VarMap: expr -> expr -> expr -> Prop :=
| VarMapBase: forall body sub eout sub' body', VarShiftUp sub 0 sub' -> VarSubst body 0 sub' body' -> VarShiftDo body' 0 eout -> VarMap (Lam body) sub eout
.

Inductive NotFreeVar: expr -> nat -> Prop :=
| NFUnt: forall n, NotFreeVar Unt n
| NFVar: forall v n, v <> n -> NotFreeVar (Var v) n
| NFLam: forall body n, NotFreeVar body (S n) -> NotFreeVar (Lam body) n
| NFApp: forall f arg n, NotFreeVar f n -> NotFreeVar arg n -> NotFreeVar (App f arg) n
| NFAnt: forall e annot n, NotFreeVar e n -> NotFreeVar (Ant e annot) n
.

Inductive NotFreeUni: type -> nat -> Prop :=
| NFTUnt: forall n, NotFreeUni TUnt n
| NFTFun: forall tl tr n, NotFreeUni tl n -> NotFreeUni tr n -> NotFreeUni (TFun tl tr) n
| NFTUni: forall v n, v <> n -> NotFreeUni (TUni v) n
| NFTAll: forall tabs n, NotFreeUni tabs (S n) -> NotFreeUni (TAll tabs) n
| NFTExV: forall v n, NotFreeUni (TExV v) n
.
