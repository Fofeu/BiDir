From BiDir Require Import Lang.

Inductive BEKind := KBeta | KEta.

Inductive BetaEta: expr -> expr -> Prop :=
| Beta: forall body arg body', VarMap (Lam body) arg body' -> BetaEta (App (Lam body) arg) body'
| Eta: forall body body', NotFreeVar body 0 -> VarShiftDo body 0 body' -> BetaEta (Lam (App body (Var 0))) body'
.

Inductive BEta: expr -> BEKind -> expr -> Prop :=
| BEBeta: forall body arg body', VarMap (Lam body) arg body' -> BEta (App (Lam body) arg) KBeta body'
| BEEta: forall body body', NotFreeVar body 0 -> VarShiftDo body 0 body' -> BEta (Lam (App body (Var 0))) KEta body'
| BEAppF: forall f arg k f', BEta f k f' -> BEta (App f arg) k (App f' arg)
| BEAppA: forall f arg k arg', BEta arg k arg' -> BEta (App f arg) k (App f arg')
| BELam: forall body k body', BEta body k body' -> BEta (Lam body) k (Lam body')
.

Inductive BEtas: expr -> nat -> expr -> Prop :=
| BEta0: forall e, BEtas e 0 e
| BEtaS: forall e n e' k e'', BEtas e n e' -> BEta e' k e'' -> BEtas e (S n) e''
.

Inductive BEtaEq: expr -> expr -> Prop := BEEq: forall l r e n m, BEtas l n e -> BEtas r m e -> BEtaEq l r.
