From BiDir.Lemmas Require Import BEtaEqRefl.
From BiDir.Lemmas Require Import BEtasLam.
From BiDir.Lemmas Require Import BEtasChainApp.
From BiDir.Lemmas Require Import IdNoBEta.
From BiDir.Lemmas Require Import AppIdBEtas.

Module BEtas.
  Definition Lam := BEtasLam.

  Module Chain.
    Definition App := BEtasChainApp.
  End Chain.

  Module Id.
    Definition BEta0 := IdNoBEta.
    Definition App := AppIdBEtas.
  End Id.
End BEtas.

Module BEtaEq.
  Definition Refl := BEtaEqRefl.
End BEtaEq.
