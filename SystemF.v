From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Inductive SysFInf: ctx -> expr -> type -> Prop :=
| SysFUnt: forall ctx, DeCtxWF ctx -> SysFInf ctx Unt TUnt
| SysFVar: forall ctx v t, DeCtxWF ctx -> DeCtxInVar ctx v t -> SysFInf ctx (Var v) t
| SysFLam: forall ctx body tl tr, DeCtxWF ctx -> SysFInf (BVar tl::ctx) body tr -> SysFInf ctx (Lam body) (TFun tl tr)
| SysFApp: forall ctx f arg tl tr, DeCtxWF ctx -> SysFInf ctx f (TFun tl tr) -> SysFInf ctx arg tl -> SysFInf ctx (App f arg) tr
| SysFAbI: forall ctx e t, DeCtxWF ctx -> SysFInf (BUni::ctx) e t -> SysFInf ctx e (TAll t)
| SysFAbE: forall ctx e t tau t', DeCtxWF ctx -> SysFInf ctx e (TAll t) -> DeTypeWF ctx tau -> Monotype tau -> UniMap (TAll t) tau t'  -> SysFInf ctx e t'
.
