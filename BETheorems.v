Theorem Progress:
  forall ctx e t,
    DeCtxNoBVar ctx -> SysFInf ctx e t -> Value e \/ exists k e', BEta e k e'.
Proof.
  intros ctx e t HNoBVar HSysF.
  induction HSysF.
  - now repeat constructor.
  - assert (HFalse: False) by (apply (NoBVarCtxInFalse ctx v t);auto);
      contradiction.
  - now repeat constructor.
  - right.
    destruct (IHHSysF1 HNoBVar) as [ HValuef | [ k [ f' HBEtaf ] ] ].
    + inversion HValuef; subst.
      * assert (HProof: MonotypeRoot (TFun tl tr) TUnt) by (apply (MonotypeRootTUntSysFInf ctx); auto);
          inversion HProof.
      * destruct (VarMapDecidable body arg) as [ body' HVarMap ].
        eexists.
        exists body'.
        now constructor.
    + exists k.
      exists (App f' arg).
      now constructor.
  - destruct IHHSysF as [ HValue | [ k [ e' HBEta ] ] ]; try now constructor.
     right; exists k; exists e'; auto.
  - destruct IHHSysF as [ HValue | [ k [ e' HBEta ] ] ]; auto.
    right; exists k; exists e'; auto.
Qed.

Theorem Preservation:
  forall ctx e t,
    SysFInf ctx e t -> forall k e', BEta e k e' -> SysFInf ctx e' t.
Proof.
  intros ctx e t HSysF.
  induction HSysF.
  - intros k e' HBEta; inversion HBEta.
  - intros k e' HBEta; inversion HBEta.
  - intros k e' HBEta.
    inversion HBEta; subst.
    + admit.
    + constructor; auto.
      now apply (IHHSysF k body').
  - intros k e' HBEta.
    inversion HBEta; subst.
    + inversion H4; subst;
        rename sub' into arg';
        rename H1 into HShiftUp; rename H2 into HSubst; rename H3 into HShiftDo.
      assert (HNFarg': NotFreeVar arg' 0) by (now apply (VarShiftUpNotFree arg)).
      assert (HNFbody': NotFreeVar body' 0) by (now apply (VarSubstNotFree body 0 arg')).
      inversion HSysF1; subst.
      * admit.
      * admit.
    + apply (SysFApp _ _ _ tl); auto.
      now apply (IHHSysF1 k).
    + apply (SysFApp _ _ _ tl); auto.
      now apply (IHHSysF2 k).
  - intros k e' HBEta.
    apply SysFAbI; auto.
    now apply (IHHSysF k).
  - intros k e' HBEta.
    apply (SysFAbE _ _ t tau); auto.
    now apply (IHHSysF k).
Admitted.

Theorem Preservations:
  forall e n e', BEtas e n e' -> forall ctx t, SysFInf ctx e t -> SysFInf ctx e' t.
Proof.
  intros e n e' HBEtas.
  induction HBEtas.
  - auto.
  - intros ctx t HSysF.
    now apply (Preservation ctx e' t (IHHBEtas ctx t HSysF) k).
Qed.

Theorem Termination:
  forall ctx e t,
    DeCtxNoBVar ctx -> SysFInf ctx e t -> (exists n e', BEtas e n e' /\ Value e').
Proof.
  intros ctx e t HNoBVar HSysF.
  induction HSysF.
  - exists 0, Unt; repeat constructor.
  - assert (HFalse: False) by (apply (NoBVarCtxInFalse ctx v t); auto);
      contradiction.
  - exists 0, (Lam body); repeat constructor.
  - destruct (IHHSysF1 HNoBVar) as [ nf [ f' [ HBEtasf HValuef ] ] ].
    destruct (IHHSysF2 HNoBVar) as [ narg [ arg' [ HBEtasarg HValuearg ] ] ].
    assert (HSysF1': SysFInf ctx f' (TFun tl tr)) by (now apply (Preservations f nf)).
    inversion HValuef; subst;
      (try (assert (HProof: MonotypeRoot (TFun tl tr) TUnt) by (now apply (MonotypeRootTUntSysFInf ctx));
           now inversion HProof)).
    inversion HBEtasf; subst.
    +
      inversion HBEtasarg; subst.
      * destruct (VarMapDecidable body arg') as [ body' HVarMap ].
        admit.
      * admit.
    + admit.
  - apply IHHSysF; constructor; auto.
  - now apply IHHSysF.
Abort.
