From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Lemma DeCtxCutBVarDeCtxMemUni:
  forall ctx v, DeCtxMemUni ctx v -> forall n ctx', DeCtxCutBVar ctx n ctx' -> DeCtxMemUni ctx' v.
Proof.
  intros ctx v HCtxMem.
  induction HCtxMem.
  - intros n ctx' HCtxCut.
    inversion HCtxCut; constructor.
  - intros n ctx' HCtxCut.
    inversion HCtxCut; subst; auto.
    constructor.
    now apply (IHHCtxMem n0).
  - intros n ctx' HCtxCut.
    inversion HCtxCut; subst.
    constructor.
    now apply (IHHCtxMem n).
Qed.
