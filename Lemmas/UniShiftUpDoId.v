From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftUpDoId:
  forall t t' n,
    UniShiftUp t n t' -> forall t'', UniShiftDo t' n t'' -> t = t''.
Proof.
  intros t t' n HShiftUp.
  induction HShiftUp; subst.
  - intros t'' HShiftDo; inversion HShiftDo; reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    + apply IHHShiftUp1.
      inversion HShiftDo; auto.
    + apply IHHShiftUp2.
      inversion HShiftDo; auto.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    simpl; reflexivity.
    lia.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst; try reflexivity.
    lia.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    apply IHHShiftUp.
    auto.
  - intros t'' HShiftDo.
    inversion HShiftDo; reflexivity.
Qed.
