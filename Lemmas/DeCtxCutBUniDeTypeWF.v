From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import
  DeCtxCutBUniDeCtxMemUniLt
  DeCtxCutBUniDeCtxMemUniGE.

Lemma DeCtxCutBUniDeTypeWF:
  forall ctx t, DeTypeWF ctx t -> forall n ctx' t', DeCtxCutBUni ctx n ctx' -> UniShiftUp t' n t  -> DeTypeWF ctx' t'.
Proof.
  intros ctx t HTypeWF.
  induction HTypeWF;
    intros n ctx' t' HCtxCut HShiftUp;
    inversion HShiftUp; subst.
  - constructor.
  - constructor; eauto.
  - constructor.
    eapply DeCtxCutBUniDeCtxMemUniGE; eauto.
  - constructor.
    eapply DeCtxCutBUniDeCtxMemUniLt; eauto.
  - constructor.
    eapply (IHHTypeWF (S n));
      try constructor;
      eauto.
Qed.
