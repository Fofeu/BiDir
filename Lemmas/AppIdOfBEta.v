From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From BiDir.Lemmas Require Import AppIdBEta.

Lemma AppIdOfBEta:
  forall e id k, BEta id k (Lam (Var 0)) -> BEtas (App id e) 2 e.
Proof.
  intros e id k HBEtaid.
  apply (BEtaS _ 1 (App (Lam (Var 0)) e) KBeta).
  + apply (BEtaS _ 0 (App id e) k); now constructor.
  + apply AppIdBEta.
Qed.
