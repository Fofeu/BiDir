From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From BiDir.Lemmas Require Import AppIdOfBEtas.

Lemma AppIdBEtas:
  forall e n e', BEtas e n e' -> forall id m, BEtas id m (Lam (Var 0)) -> BEtas (App id e) (S (n + m)) e'.
Proof.
  intros e n e' HBEtas.
  induction HBEtas.
  - intros id m HBEtasid.
    now apply AppIdOfBEtas.
  - intros id m HBEtasid.
    eapply BEtaS; eauto.
Qed.
