From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftUpDoUniq:
  forall e threshold e', VarShiftUp e threshold e' -> forall e'', VarShiftDo e'' threshold e -> NotFreeVar e'' threshold -> e' = e''.
Proof.
  intros e threshold e' HShiftUp.
  induction HShiftUp;
    intros e'' HShiftDo HNFree.
  - inversion HShiftDo; subst.
    reflexivity.
  - inversion HShiftDo; subst; try lia.
    reflexivity.
  - inversion HShiftDo; subst; try reflexivity.
    inversion HNFree; try lia.
  - inversion HShiftDo; subst.
    inversion HNFree; subst.
    f_equal.
    now apply IHHShiftUp.
  - inversion HShiftDo; subst.
    inversion HNFree; subst.
    f_equal.
    now apply IHHShiftUp1.
    now apply IHHShiftUp2.
  - inversion HShiftDo; subst.
    inversion HNFree; subst.
    f_equal.
    now apply IHHShiftUp.
Qed.
