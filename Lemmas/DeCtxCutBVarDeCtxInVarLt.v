From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxCutBVarDeCtxInVarLt:
  forall ctx n ctx', DeCtxCutBVar ctx n ctx' -> forall v t, DeCtxInVar ctx v t -> v < n -> DeCtxInVar ctx' v t.
Proof.
  intros ctx n ctx' HCtxCut.
  induction HCtxCut.
  - lia.
  - intros v t HCtxIn HLt.
    destruct v.
    + inversion HCtxIn; subst.
      constructor.
    + inversion HCtxIn; subst.
      constructor.
      apply IHHCtxCut; auto; lia.
  - intros v t HCtxIn HLt.
    inversion HCtxIn; subst.
    econstructor; eauto.
Qed.
