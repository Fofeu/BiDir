From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.
From Hammer Require Import Tactics Hammer.

From BiDir.Lemmas Require Import
  VarShiftUpDecidable
  VarSubstDecidable
  VarShiftUpNotFree.

Lemma BEtaLam:
  forall e k e', BEta e k e' -> BEta (Lam e) k (Lam e').
Proof.
  intros.
  now repeat constructor.
Qed.
