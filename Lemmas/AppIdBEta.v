From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From BiDir.Lemmas Require Import VarShiftUpDecidable.
From BiDir.Lemmas Require Import VarShiftDoDecidable.
From BiDir.Lemmas Require Import VarShiftUpDoId.
From BiDir.Lemmas Require Import VarShiftDoSafeUp.

Lemma AppIdBEta:
  forall e, BEta (App (Lam (Var 0)) e) KBeta e.
Proof.
  intros e.
  constructor.
  destruct (VarShiftUpDecidable e 0) as [ e' HShiftUp ].
  assert (HShiftDoSafe: VarShiftDoSafe e' 0) by now apply (VarShiftDoSafeUp e 0 e').
  destruct (VarShiftDoDecidable e' 0 HShiftDoSafe) as [ e'' HShiftDo ].
  assert (Heq: e = e'') by (eapply VarShiftUpDoId; eauto); subst.
  econstructor; eauto; econstructor.
Qed.
