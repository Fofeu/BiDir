From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpDec.

Lemma MonotypeRootTUntSubst:
  forall t,
    MonotypeRoot t TUnt -> forall n sub, UniSubst t n sub t.
Proof.
  intros t HMRoot.
  remember TUnt as tunt.
  induction HMRoot; try discriminate.
  - subst; constructor.
  - subst.
    intros n sub.
    destruct (UniShiftUpDec sub 0) as [ sub' HShiftUp ].
    apply (UniSubstTAll _ _ _ sub'); auto.
Qed.
