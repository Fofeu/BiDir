From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import DeCtxInsertBUniCtxMem.

Lemma DeCtxInsertBUniTypeWF:
  forall ctx t, DeTypeWF ctx t -> forall n ctx', DeCtxInsertBUni ctx n ctx' -> DeTypeWF ctx' t.
Proof.
  intros ctx t HTypeWF.
  induction HTypeWF.
  - constructor.
  - constructor.
    now apply (IHHTypeWF1 n).
    now apply (IHHTypeWF2 n).
  - intros n ctx' HCtxInsert.
    constructor.
    eapply DeCtxInsertBUniCtxMem; eauto.
  - intros n ctx' HCtxInsert.
    constructor.
    apply (IHHTypeWF (S n)).
    now constructor.
Qed.
