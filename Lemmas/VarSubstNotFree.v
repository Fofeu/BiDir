From BiDir Require Import Lang.

From BiDir.Lemmas Require Import VarShiftUpNotFreeTwice.

From Coq Require Import Lia.

Lemma VarSubstNotFree:
  forall e n sub e', VarSubst e n sub e' -> NotFreeVar sub n -> NotFreeVar e' n.
Proof.
  intros e n sub e' HSubst HNFree.
  induction HSubst.
  - constructor.
  - auto.
  - constructor; lia.
  - constructor.
    apply IHHSubst.
    apply (VarShiftUpNotFreeTwice sub 0); auto.
    lia.
  - constructor.
    now apply IHHSubst1.
    now apply IHHSubst2.
  - constructor.
    now apply IHHSubst.
Qed.
