From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma DeCtxInsertBUniExtended:
  forall ctx n ctx',
    DeCtxInsertBUni ctx n ctx' -> DeCtxInsertBUni (BUni :: ctx) (S n) (BUni :: ctx').
Proof.
  intros ctx n ctx' HCtxInsert.
  induction HCtxInsert.
  - repeat constructor.
  - now repeat constructor.
  - now repeat constructor.
Qed.
