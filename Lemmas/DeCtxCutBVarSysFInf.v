From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import SystemF.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import DeCtxCutBVarDeCtxWF.
From BiDir.Lemmas Require Import DeCtxCutBVarDeTypeWF.
From BiDir.Lemmas Require Import DeCtxCutBVarDeCtxInVarGE.
From BiDir.Lemmas Require Import DeCtxCutBVarDeCtxInVarLt.

Lemma DeCtxCutBVarSysFInf:
  forall ctx e t,
    SysFInf ctx e t -> forall n ctx' e', DeCtxCutBVar ctx n ctx' -> NotFreeVar e n -> VarShiftDo e n e' -> SysFInf ctx' e' t.
Proof.
  intros ctx e t HSysF.
  induction HSysF;
    intros n ctx' e' HCutBVar HNFree HShiftDo;
    assert (HCCtxWF: DeCtxWF ctx') by now apply (DeCtxCutBVarDeCtxWF ctx n).
  - inversion HShiftDo; subst; now constructor.
  - inversion HShiftDo; subst; constructor; auto.
    + inversion HNFree; subst.
      apply (DeCtxCutBVarDeCtxInVarGE ctx n); auto.
      lia.
    + now apply (DeCtxCutBVarDeCtxInVarLt ctx n).
  - inversion HShiftDo; subst.
    inversion HNFree; subst.
    constructor; auto.
    apply (IHHSysF (S n)); auto.
    now constructor.
  - inversion HShiftDo; subst.
    inversion HNFree; subst.
    apply (SysFApp _ _ _ tl); auto.
    now apply (IHHSysF1 n).
    now apply (IHHSysF2 n).
  - apply SysFAbI; auto.
    apply (IHHSysF n); auto.
    now constructor.
  - apply (SysFAbE _ _ t tau); auto.
    apply (IHHSysF n); auto.
    now apply (DeCtxCutBVarDeTypeWF ctx _ H0 n).
Qed.
