From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Lemma DeCtxInsertBVarBUniExtended:
  forall ctx n t ctx', DeCtxInsertBVar ctx n t ctx' -> DeCtxInsertBVar (BUni :: ctx) n t (BUni :: ctx').
Proof.
  intros ctx n t ctx' HCtxInsert.
  induction HCtxInsert.
  - repeat constructor.
  - constructor.
    constructor.
    now inversion IHHCtxInsert.
  - now constructor.
Qed.
