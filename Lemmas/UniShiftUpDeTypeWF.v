From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import
  DeCtxInsertBUniDeCtxMemUni
  DeCtxInsertBUniExtended.

Lemma UniShiftUpDeTypeWF:
  forall t n t', UniShiftUp t n t' -> forall ctx ctx', DeTypeWF ctx t -> DeCtxInsertBUni ctx n ctx' -> DeTypeWF ctx' t'.
Proof.
  intros t n t' HShiftUp.
  induction HShiftUp.
  - constructor.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    now apply (IHHShiftUp1 ctx).
    now apply (IHHShiftUp2 ctx).
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    eapply DeCtxInsertBUniDeCtxMemUni; eauto.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    eapply DeCtxInsertBUniDeCtxMemUni; eauto.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    apply (IHHShiftUp (BUni:: ctx)); auto.
    eapply DeCtxInsertBUniExtended; eauto.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF.
Qed.
