From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import DeCtxCutBVarDeTypeWF.

Lemma DeCtxCutBVarDeCtxWF:
  forall ctx n ctx', DeCtxWF ctx -> DeCtxCutBVar ctx n ctx' -> DeCtxWF ctx'.
Proof.
  intros ctx n ctx' HCtxWF HCtxCut.
  induction HCtxCut.
  - now inversion HCtxWF.
  - assert (HRCtxWF: DeCtxWF ctx) by now inversion HCtxWF.
    assert (HRCtxWF': DeCtxWF ctx') by apply (IHHCtxCut HRCtxWF).
    assert (HTypeWFtv: DeTypeWF ctx tv) by now inversion HCtxWF.
    constructor; auto.
    now apply (DeCtxCutBVarDeTypeWF ctx _ HTypeWFtv n).
  - inversion HCtxWF; constructor; auto.
Qed.
