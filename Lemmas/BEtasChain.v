From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.

Lemma BEtasChain:
  forall e n e' m e'', BEtas e n e' -> BEtas e' m e'' -> BEtas e (n + m) e''.
Proof.
  intros e n e' m e'' HBEtase HBEtase'.
  generalize e n HBEtase; clear e n HBEtase.
  induction HBEtase'.
  - intros e' n HBEtas.
    replace (n + 0) with n by lia.
    auto.
  - intros e''' m HBEtase'''.
    replace (m + S n) with (S (m + n)) by lia.
    eapply BEtaS; try now apply H.
    now apply IHHBEtase'.
Qed.
