From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma VarShiftUpDecidable:
  forall e threshold, exists e', VarShiftUp e threshold e'.
Proof.
  intros e.
  induction e.
  - exists Unt.
    constructor.
  - intros threshold.
    destruct (PeanoNat.Nat.le_gt_cases threshold n).
    + exists (Var (S n)).
      now constructor.
    + exists (Var n).
      now constructor.
  - intros threshold.
    destruct (IHe (S threshold)) as [ e' HShiftUp ].
    exists (Lam e').
    now constructor.
  - intros threshold.
    destruct (IHe1 threshold) as [ e1' HShiftUpe1 ].
    destruct (IHe2 threshold) as [ e2' HShiftUpe2 ].
    exists (App e1' e2').
    now constructor.
  - intros threshold.
    destruct (IHe threshold) as [ e' HShiftUp ].
    exists (Ant e' t).
    now constructor.
Qed.
