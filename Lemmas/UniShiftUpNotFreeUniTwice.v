From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftUpNotFreeUniTwice:
  forall t n t', UniShiftUp t n t' -> forall m, NotFreeUni t m -> n < (S m) -> NotFreeUni t' (S m).
Proof.
  intros t n t' HShiftUp.
  induction HShiftUp.
  - constructor.
  - intros m HNFree HLt.
    inversion HNFree; subst.
    constructor.
    now apply (IHHShiftUp1 m).
    now apply (IHHShiftUp2 m).
  - intros m HNFree HLt.
    constructor.
    inversion HNFree; subst.
    lia.
  - intros m HNFree HLt.
    constructor.
    inversion HNFree; subst.
    lia.
  - intros m HNFree HLt.
    inversion HNFree; subst.
    constructor.
    apply (IHHShiftUp (S m)); auto.
    lia.
  - constructor.
Qed.
