From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import SystemF.

Lemma SysFMoveBUniTAll:
  forall ctx e t,
    SysFInf (BUni :: ctx) e t -> SysFInf ctx e (TAll t).
Proof.
  intros ctx e t HSysF.
  assert (HECtxWF: DeCtxWF (BUni :: ctx)) by (inversion HSysF; auto).
  assert (HCtxWF: DeCtxWF ctx) by (inversion HECtxWF; auto).
  now apply SysFAbI.
Qed.
