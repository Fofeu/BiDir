From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import UniShiftUpDeTypeWF.

Lemma UniSubstTypeWF:
  forall t n sub t', UniSubst t n sub t' -> forall ctx, DeTypeWF ctx t -> DeTypeWF ctx sub -> DeTypeWF ctx t'.
Proof.
  intros t n sub t' HSubst.
  induction HSubst.
  - constructor.
  - intros ctx HTypeWF HTypeWFsub.
    inversion HTypeWF; subst.
    constructor.
    now apply IHHSubst1.
    now apply IHHSubst2.
  - auto.
  - auto.
  - intros ctx HTypeWF HTypeWFsub.
    inversion HTypeWF; subst.
    constructor.
    apply IHHSubst; auto.
    apply (UniShiftUpDeTypeWF sub 0 sub' H ctx); auto.
    repeat constructor.
  - intros ctx HTypeWF.
    inversion HTypeWF.
Qed.
