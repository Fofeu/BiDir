From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.

Lemma DeCtxInsertBVarDeCtxInVarPre:
  forall ctx n tn ctx', DeCtxInsertBVar ctx n tn ctx' -> forall v tv, DeCtxInVar ctx v tv -> v < n -> DeCtxInVar ctx' v tv.
Proof.
  intros ctx n tn ctx' HCtxInsert.
  induction HCtxInsert as
    [ ctx t
    | ctx n tn ctx' tv' HCtxInsert IHHCtxInsert
    | ctx n tn ctx' HCtxInsert IHHCtxInsert
    ];
    intros v tv HCtxIn HPre.
  - lia.
  - inversion HCtxIn; subst.
    constructor.
    constructor.
    apply IHHCtxInsert; auto; lia.
  - inversion HCtxIn; subst.
    econstructor; eauto.
Qed.
