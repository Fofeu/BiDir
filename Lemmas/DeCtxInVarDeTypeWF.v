From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpDeTypeWF.
From BiDir.Lemmas Require Import DeCtxInsertBVarDeTypeWF.

Lemma DeCtxInVarDeTypeWF:
  forall ctx v t,
    DeCtxWF ctx -> DeCtxInVar ctx v t -> DeTypeWF ctx t.
Proof.
  intros ctx v t HCtxWF HCtxIn.
  induction HCtxIn.
  - inversion HCtxWF; subst.
    apply (DeCtxInsertBVarDeTypeWF ctx _ H2 0 t).
    constructor.
  - assert (HRCtxWF: DeCtxWF ctx) by now inversion HCtxWF.
    assert (HTypeWF: DeTypeWF ctx t) by now apply IHHCtxIn.
    apply (DeCtxInsertBVarDeTypeWF ctx _ HTypeWF 0 t').
    constructor.
  - apply (UniShiftUpDeTypeWF t 0 _ H ctx).
    apply IHHCtxIn; now inversion HCtxWF.
    constructor.
Qed.
