From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import
  DeCtxCutBVarDeTypeWF
  DeSubDeTypeWF.

Lemma DeChkDeTypeWF:
  forall ctx e t,
    DeChk ctx e t -> DeTypeWF ctx t.
Proof.
  intros ctx e t HChk.
  induction HChk.
  - constructor.
  - constructor.
    inversion HChk; subst;
      inversion H0; subst;
      auto.
    eapply DeCtxCutBVarDeTypeWF; eauto.
    constructor.
  - now destruct (DeSubDeTypeWF ctx tsyn t H1).
  - constructor; auto.
Qed.
