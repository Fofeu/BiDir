From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import SystemF.
From BiDir Require Import BetaEta.

From BiDir.Lemmas Require Import SysFInfTypeWF.

Lemma AppIdPreserves:
  forall ctx e t,
    SysFInf ctx e t -> SysFInf ctx (App (Lam (Var 0)) e) t.
Proof.
  intros ctx e t HSysF.
  assert (HCtxWF: DeCtxWF ctx) by now inversion HSysF.
  apply (SysFApp _ _ _ t); auto.
  apply (SysFLam); auto.
  repeat constructor; auto.
  now apply (SysFInfTypeWF ctx e).
Qed.
