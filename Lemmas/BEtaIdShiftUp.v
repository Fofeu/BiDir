From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.
From Hammer Require Import Tactics.

From BiDir.Lemmas Require Import
  VarShiftUpDecidable
  VarShiftUpUniq.

Lemma BEtaIdShiftUp:
  forall e k, BEta e k (Lam (Var 0)) -> forall e', VarShiftUp e 0 e' -> BEtas (Lam e') 1 (Lam (Lam (Var 0))).
Proof.
  intros e k HBEta.
  inversion HBEta; subst.
  - intros e' HShiftUp.
    inversion HShiftUp; subst.
    inversion H2; subst.
    inversion H; subst.
    inversion H6; subst.
    inversion H4; subst.
    + inversion H1; subst; try lia.
      inversion H3; subst.
      inversion H9; subst; try lia.
      * inversion H11; subst; try lia.
      * inversion H11; subst.
        inversion H5; subst.
        inversion H8; subst; try lia.
        eapply BEtaS.
        eapply BEta0.
        eapply BELam.
        eapply BEBeta.
        sfirstorder.
    + inversion H1; subst.
      inversion H6; subst.
      inversion H9; subst.
      * inversion H12; subst; try lia.
        inversion H8; subst; try lia.
        eapply BEtaS.
        eapply BEta0.
        eapply BELam.
        eapply BEBeta.
        sauto lq: on.
      * inversion H4; subst.
        inversion H12; subst.
        -- inversion H8; subst; try lia.
           assert (sub' = arg') by (eapply VarShiftUpUniq; eauto); subst.
           eapply BEtaS.
           eapply BEta0.
           eapply BELam.
           eapply BEBeta.
           sauto lq: on drew: off.
        -- inversion H8; subst; try lia.
           destruct (VarShiftUpDecidable arg' 0) as [ arg'' HShiftUparg'' ].
           destruct (VarShiftUpDecidable arg'' 0) as [ arg''' HShiftUparg''' ].
           eapply BEtaS.
           eapply BEta0.
           eapply BELam.
           eapply BEBeta.
           econstructor.
           eapply HShiftUparg''.
           econstructor.
           eapply HShiftUparg'''.
           econstructor; try lia.
           econstructor.
           econstructor; try lia.
  - intros e' HShiftUp.
    inversion HShiftUp; subst.
    inversion H2; subst.
    inversion H7; subst; try lia.
    inversion H0; subst.
    inversion H4; subst.
    inversion H8; subst.
    + inversion H5; subst; try lia.
      eapply BEtaS.
      eapply BEta0.
      eapply BELam.
      eapply BELam.
      eapply BEBeta.
      econstructor.
      econstructor; lia.
      econstructor; lia.
      econstructor; lia.
    + inversion H5; subst; try lia.
      eapply BEtaS.
      eapply BEta0.
      eapply BELam.
      eapply BELam.
      eapply BEBeta.
      sauto lq: on.
  - intros e' HShiftUp.
    inversion HShiftUp; subst.
    inversion H2; subst.
    + inversion H0; subst.
      inversion H; subst.
      inversion H4; subst.
      inversion H6; subst; try lia.
      inversion H5; subst.
      * inversion H8; subst; try lia.
        inversion H3; subst; try lia.
        inversion H7; subst; try lia.
        sauto lq: on.
        (* apply (BEtaS _ _ (Lam (Lam (App (Lam (Var 0)) (Var 0)))) KBeta). *)
        (* constructor. *)
        (* apply BELam. *)
        (* apply BELam. *)
        (* apply BEBeta. *)
        (* sfirstorder. *)
      * inversion H8; subst; try lia.
        eapply (BEtaS).
        apply BEta0.
        apply BELam.
        apply BELam.
        apply BEBeta.
        destruct (VarShiftUpDecidable arg' 0) as [ arg'' HShiftUparg'' ].
        apply (VarMapBase _ _ _ arg'' (Var 1)).
        auto.
        constructor; lia.
        constructor; lia.
    + inversion H0; subst.
      inversion H1; subst; try lia.
      inversion H4; subst.
      inversion H6; subst; try lia.
      inversion H10; subst; try lia.
      eapply BEtaS.
      apply BEta0.
      apply BELam.
      apply BELam.
      apply BEEta.
      constructor; lia.
      constructor; lia.
Qed.
