From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.

Lemma EtaOfErasure:
  forall e e', Erasure e e' -> exists e'', BEta e'' KEta e'.
Proof.
  intros e e' HErasure.
  induction HErasure.
  - exists (Lam (App Unt (Var 0))); repeat constructor.
  - exists (Lam (App (Var (S v)) (Var 0))).
    apply BEEta; repeat constructor; lia.
  - destruct IHHErasure as [ e'' HBEta ].
    exists (Lam e'').
    now apply BELam.
  - destruct IHHErasure1 as [ f'' HBEtaf ].
    destruct IHHErasure2 as [ arg'' HBEtaarg ].
    exists (App f'' arg').
    now apply BEAppF.
  - destruct IHHErasure as [ e'' HBEta ].
    now exists e''.
Qed.
