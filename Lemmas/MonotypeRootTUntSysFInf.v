From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import SystemF.

From BiDir.Lemmas Require Import MonotypeRootTUntMap.
From BiDir.Lemmas Require Import UniMapUniq.

Lemma MonotypeRootTUntSysFInf:
  forall ctx t,
    SysFInf ctx Unt t -> MonotypeRoot t TUnt.
Proof.
  remember Unt as e.
  intros ctx t HSysF.
  induction HSysF; try discriminate; subst.
  + constructor.
  + constructor.
    apply IHHSysF; auto.
  + assert (HMRoot: MonotypeRoot (TAll t) TUnt) by apply (IHHSysF eq_refl).
    assert (HMap: UniMap (TAll t) tau t) by (apply MonotypeRootTUntMap; inversion HMRoot; auto).
    assert (Heqt: t' = t) by (apply (UniMapUniq t tau t' H2 t HMap));
      subst.
    inversion HMRoot; auto.
Qed.
