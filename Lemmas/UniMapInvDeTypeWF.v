From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import UniSubstInvDeTypeWF.
From BiDir.Lemmas Require Import UniShiftDoInvDeTypeWF.
From BiDir.Lemmas Require Import UniShiftUpDeTypeWF.

Lemma UniMapInvDeTypeWF:
  forall ctx t sub t',
    DeTypeWF ctx t' -> DeTypeWF ctx sub -> UniMap t sub t' -> DeTypeWF ctx t.
Proof.
  intros ctx t sub t' HTypeWF HTypeWFsub HUniMap.
  destruct HUniMap as [ tabs sub tout sub' tabs' HShiftUp HSubst HShiftDo ].
  constructor.
  apply (UniSubstInvDeTypeWF tabs 0 sub' tabs'); auto.
  + apply (UniShiftDoInvDeTypeWF tabs' 0  tout HShiftDo ctx); auto.
    repeat constructor.
  + apply (UniShiftUpDeTypeWF sub 0 sub' HShiftUp ctx); auto.
    repeat constructor.
  + repeat constructor.
Qed.
