From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma BEtaEqRefl:
  forall e, BEtaEq e e.
Proof.
  intros e.
  now repeat econstructor.
Qed.
