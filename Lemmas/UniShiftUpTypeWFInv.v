From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import
  DeCtxCutBUniCtxMemGE
  DeCtxCutBUniOfInsert
  DeCtxInsertBUniExtended
  UniMemLt.

Lemma UniShiftUpTypeWFInv:
  forall t n t', UniShiftUp t n t' -> forall ctx ctx', DeTypeWF ctx' t' -> DeTypeWF ctx (TUni n) -> DeCtxInsertBUni ctx n ctx' -> DeTypeWF ctx t.
Proof.
  intros t n t' HShiftUp.
  induction HShiftUp.
  - constructor.
  - intros ctx ctx' HTypeWF HTypeWFUni HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    now apply (IHHShiftUp1 _ ctx').
    now apply (IHHShiftUp2 _ ctx').
  - intros ctx ctx' HTypeWF HTypeWFUni HCtxInsert.
    inversion HTypeWF; subst.
    assert (HCtxCut: DeCtxCutBUni ctx' threshold ctx) by now apply DeCtxCutBUniOfInsert.
    constructor.
    eapply DeCtxCutBUniCtxMemGE; eauto.
  - intros ctx ctx' HTypeWF HTypeWFUni HCtxInsert.
    inversion HTypeWF; subst.
    inversion HTypeWFUni; subst.
    constructor.
    apply (UniMemLt ctx threshold); auto; lia.
  - intros ctx ctx' HTypeWF HTypeWFUni HCtxExtension.
    inversion HTypeWF; subst.
    inversion HTypeWFUni; subst.
    constructor.
    apply (IHHShiftUp (BUni :: ctx) (BUni :: ctx')); auto.
    now repeat constructor.
    now apply DeCtxInsertBUniExtended.
  - intros ctx ctx' HTypeWF.
    inversion HTypeWF.
Qed.
