From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftDoUniq:
  forall t threshold t', UniShiftDo t threshold t' -> forall t'', UniShiftDo t threshold t'' -> t' = t''.
Proof.
  intros t threshold t' HShiftDo.
  induction HShiftDo.
  - intros t'' HShiftDo.
    inversion HShiftDo.
    reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    apply IHHShiftDo1; auto.
    apply IHHShiftDo2; auto.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst; try lia.
    reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst; try lia.
    reflexivity.
  - intros t'' HShiftDo'.
    inversion HShiftDo'; subst.
    f_equal.
    apply IHHShiftDo; auto.
  - intros t'' HShiftDo.
    inversion HShiftDo.
    reflexivity.
Qed.
