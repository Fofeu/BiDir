 From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import DeCtxInsertBUniDeCtxMemUni.
From BiDir.Lemmas Require Import DeCtxInsertBUniExtended.

Lemma UniShiftDoInvDeTypeWF:
  forall t n t', UniShiftDo t n t' -> forall ctx ctx', DeTypeWF ctx t' -> DeCtxInsertBUni ctx n ctx' -> DeTypeWF ctx' t.
Proof.
  intros t n t' HShiftDo.
  induction HShiftDo.
  - constructor.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    now apply (IHHShiftDo1 ctx).
    now apply (IHHShiftDo2 ctx).
  - intros ctx ctx' HTypeWF HCtxInsert.
    constructor.
    inversion HTypeWF; subst.
    eapply DeCtxInsertBUniDeCtxMemUni; eauto.
  - intros ctx ctx' HTypeWF HCtxExtension.
    inversion HTypeWF; subst.
    constructor.
    eapply DeCtxInsertBUniDeCtxMemUni; eauto; lia.
  - intros ctx ctx' HTypeWF HCtxInsert.
    inversion HTypeWF; subst.
    constructor.
    apply (IHHShiftDo (BUni :: ctx)); auto.
    now apply DeCtxInsertBUniExtended.
  - intros ctx ctx' HTypeWF.
    inversion HTypeWF.
Qed.
