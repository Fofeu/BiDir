From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma MonotypeRootUniq:
  forall t t',
    MonotypeRoot t t' -> forall t'', MonotypeRoot t t'' -> t' = t''.
Proof.
  intros t t' HMRoot.
  induction HMRoot;
    try (intros t'' HMRoot; inversion HMRoot; reflexivity).
  intros t'' HMRoot'.
  inversion HMRoot'; subst.
  now apply IHHMRoot.
Qed.
