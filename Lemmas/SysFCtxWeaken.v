From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.
From BiDir Require Import SystemF.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import
  DeCtxInsertBVarDeCtxInVarPost
  DeCtxInsertBVarDeCtxInVarPre
  DeCtxInsertBVarDeTypeWF.

Lemma SysFCtxWeakenBVar:
  forall ctx e t, SysFInf ctx e t -> forall n tv ctx' e', DeCtxInsertBVar ctx n tv ctx' -> DeCtxWF ctx' -> VarShiftUp e n e' -> SysFInf ctx' e' t.
Proof.
  intros ctx e t HSysF.
  induction HSysF;
    intros n tv ctx' e' HCtxInsert HICtxWF HShiftUp.
  - inversion HShiftUp; subst; now constructor.
  - inversion HShiftUp; subst; constructor; auto.
    + eapply DeCtxInsertBVarDeCtxInVarPost; eauto.
    + eapply DeCtxInsertBVarDeCtxInVarPre; eauto.
  - inversion HShiftUp; subst.
    assert (HECtxWF: DeCtxWF (BVar tl :: ctx)) by now inversion HSysF.
    assert (HTypeWFtl: DeTypeWF ctx tl) by now inversion HECtxWF.
    assert (HTypeWFtl': DeTypeWF ctx' tl) by (eapply DeCtxInsertBVarDeTypeWF; eauto).
    constructor; auto.
    apply (IHHSysF (S n) tv); try constructor; auto.
  - inversion HShiftUp; subst.
    econstructor; eauto.
  - apply SysFAbI; auto.
    apply (IHHSysF n tv); try constructor; auto.
  - eapply SysFAbE; eauto.
    eapply DeCtxInsertBVarDeTypeWF; eauto.
Qed.
