From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import VarShiftUpDecidable.

Lemma VarSubstDecidable:
  forall e v sub, exists e', VarSubst e v sub e'.
Proof.
  intros e.
  induction e.
  - intros; exists Unt; now constructor.
  - intros.
    destruct (PeanoNat.Nat.eq_decidable v n).
    + subst. exists sub.
      now constructor.
    + exists (Var n).
      now constructor.
  - intros.
    destruct (VarShiftUpDecidable sub 0) as [ sub' HShiftUpsub ].
    destruct (IHe (S v) sub') as [ e' HSubst ].
    exists (Lam e').
    now apply (VarSubstLam _ _ _ sub').
  - intros.
    destruct (IHe1 v sub) as [ e1' HSubste1 ].
    destruct (IHe2 v sub) as [ e2' HSubste2 ].
    exists (App e1' e2').
    now constructor.
  - intros.
    destruct (IHe v sub) as [ e' HSubst ].
    exists (Ant e' t).
    now constructor.
Qed.
