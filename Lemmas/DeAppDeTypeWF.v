From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma DeAppDeTypeWF:
  forall ctx tf e t,
    DeApp ctx tf e t -> DeTypeWF ctx t.
Proof.
  intros ctx tf e t HApp.
  induction HApp; auto.
Qed.
