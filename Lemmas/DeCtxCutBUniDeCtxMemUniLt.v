From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxCutBUniDeCtxMemUniLt:
  forall ctx v, DeCtxMemUni ctx v -> forall n ctx', DeCtxCutBUni ctx n ctx' -> v < n -> DeCtxMemUni ctx' v.
Proof.
  intros ctx v HCtxMem.
  induction HCtxMem;
    intros n ctx' HCtxCut HLt.
  - inversion HCtxCut; subst; try lia.
    constructor.
  - inversion HCtxCut; subst; auto.
    constructor.
    apply (IHHCtxMem n); auto.
  - inversion HCtxCut; subst; try lia.
    constructor.
    apply (IHHCtxMem n0); auto; lia.
Qed.
