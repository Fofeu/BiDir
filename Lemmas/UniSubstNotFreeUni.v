From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import UniShiftUpNotFreeUniTwice.

Lemma UniSubstNotFreeUni:
  forall t n sub t', UniSubst t n sub t' -> NotFreeUni sub n -> NotFreeUni t' n.
Proof.
  intros t n sub t' HSubst HNFree.
  induction HSubst.
  - constructor.
  - constructor; auto.
  - auto.
  - constructor; lia.
  - constructor.
    apply IHHSubst.
    apply (UniShiftUpNotFreeUniTwice sub 0 sub'); auto; lia.
  - constructor.
Qed.
