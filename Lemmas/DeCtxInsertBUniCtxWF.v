From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import DeCtxInsertBUniTypeWF.

Lemma DeCtxInsertBUniCtxWF:
  forall ctx n ctx', DeCtxWF ctx -> DeCtxInsertBUni ctx n ctx' -> DeCtxWF ctx'.
Proof.
  intros ctx n ctx' HCtxWF HCtxInsert.
  induction HCtxInsert.
  - now constructor.
  - inversion HCtxWF; subst.
    constructor; auto.
    eapply DeCtxInsertBUniTypeWF; eauto.
  - inversion HCtxWF; constructor; auto.
Qed.
