From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import UniShiftUpDec.

Lemma UniSubstDec:
  forall t threshold sub, exists t', UniSubst t threshold sub t'.
Proof.
  intros t.
  induction t as
    [
    | tl IHtl tr IHtr
    | v
    |
    | v
    ]
    .
  - intros.
    exists TUnt; constructor.
  - intros.
    destruct (IHtl threshold sub) as [ tl' HSubstl ].
    destruct (IHtr threshold sub) as [ tr' HSubstr ].
    exists (TFun tl' tr').
    constructor; auto.
  - intros.
    destruct (PeanoNat.Nat.lt_trichotomy threshold v) as [ HLt | [ HEq | HGt ] ].
    + exists (TUni v); constructor; lia.
    + exists sub; subst; constructor.
    + exists (TUni v); constructor; lia.
  - intros.
    destruct (UniShiftUpDec sub 0) as [ sub' HShiftUp ].
    destruct (IHt (S threshold) sub') as [ t' HSubst ].
    exists (TAll t').
    now apply (UniSubstTAll _ _ _ sub').
  - intros.
    exists (TExV v); constructor.
Qed.
