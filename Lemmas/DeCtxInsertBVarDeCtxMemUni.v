From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Lemma DeCtxInsertBVarCtxMem:
  forall ctx n t ctx', DeCtxInsertBVar ctx n t ctx' -> forall v, DeCtxMemUni ctx v -> DeCtxMemUni ctx' v.
Proof.
  intros ctx n t ctx' HCtxInsert.
  induction HCtxInsert;
    intros v HCtxMem.
  - now constructor.
  - inversion HCtxMem; subst.
    constructor.
    now apply IHHCtxInsert.
  - inversion HCtxMem; subst.
    constructor.
    constructor; auto.
Qed.
