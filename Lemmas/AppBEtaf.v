From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma AppBEtaf:
  forall f k f' arg, BEta f k f' -> BEta (App f arg) k (App f' arg).
Proof.
  now constructor.
Qed.
