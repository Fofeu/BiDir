From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.
From Hammer Require Import Tactics Hammer.

From BiDir.Lemmas Require Import BEtaIdShiftUp.

From BiDir.Lemmas Require Import
AppBEtaf
AppBEtasf
AppIdBEtas
AppIdBEta
AppIdOfBEtas
AppIdOfBEta
AppIdPreserves
BEtaChainApp
BEtaChain
BEtaEqRefl
BEtaIdShiftUp
BEtaLam
BEtasChainApp
BEtasChain
BEtasLam
ChkOfSyn
CtxInTypeWF
DeAppTypeWF
DeChkTypeWF
DeCtxBUniExtendedCut
DeCtxCutBUniCtxMemGE
DeCtxCutBUniCtxMemLt
DeCtxCutBUniTypeWF
DeCtxCutBVarCtxInGE
DeCtxCutBVarCtxInLt
DeCtxCutBVarCtxMem
DeCtxCutBVarTypeWF
DeCtxCutBVarWF
DeCtxInsertBUniCtxMem
DeCtxInsertBUniCtxWF
DeCtxInsertBUniExtended
DeCtxInsertBUniTypeWF
DeCtxInsertBVarBUniExtended
DeCtxInsertBVarCtxInPost
DeCtxInsertBVarCtxInPre
DeCtxInsertBVarCtxMem
DeCtxInsertBVarDeTypeWF
DeSubTypeWF
DeSynTypeWF
EtaOfErasure
ExtensionBUniTypeWF
IdNoBEta
LE_cases
MonotypeDecidable
MonotypeRootTUntMap
MonotypeRootTUntShiftDo
MonotypeRootTUntShiftUp
MonotypeRootTUntSubst
MonotypeRootTUntSysFInf
MonotypeRootUniq
NoBVarCtxInFalse
ShiftUpIdIntoBEta
SubRefl
SubtypingCoercion
SysFCtxWeaken
SysFInfTypeWF
SysFMoveBUniTAll
SysFOfCutBVar
UniMapTypeWFInv
UniMapTypeWF
UniMapUniq
UniMemCutLt
UniMemCutPred
UniMemInsertSucc
UniMemInsert
UniMemLt
UniMemPred
UniShiftDoDec
UniShiftDoOfUp
UniShiftDoTypeWFInv
UniShiftDoTypeWF
UniShiftDoUniq
UniShiftUpDec
UniShiftUpDoId
UniShiftUpDoTypeWF
UniShiftUpNotFreeUniTwice
UniShiftUpNotFreeUni
UniShiftUpTypeWFInv
UniShiftUpTypeWF
UniShiftUpUniq
UniSubstDec
UniSubstNotFreeUni
UniSubstTypeWFInv
UniSubstTypeWF
UniSubstUniq
VarMapDecidable
VarShiftDoDecidable
VarShiftDoOfUp
VarShiftDoSafeNFree
VarShiftDoSafeSucc
VarShiftDoSafeUp
VarShiftUpDecidable
VarShiftUpDoId
VarShiftUpDoUniq
VarShiftUpNotFreeTwice
VarShiftUpNotFree
VarShiftUpSysF
VarShiftUpUniq
VarSubstDecidable
VarSubstNotFree.

Lemma L:
  forall e n sub e', VarSubst e n sub e' -> forall e'' sub' e''', VarShiftUp e (S n) e'' -> VarShiftUp sub n sub' -> VarSubst e'' n sub' e''' -> VarShiftDo e''' n e'.
Proof.
  intros e n sub e' HSubste.
  induction HSubste;
    intros e'' subup e''' HShiftUpe HShiftUpsub HSubste''.
  - inversion HShiftUpe; subst.
    inversion HSubste''; subst.
    constructor.
  - inversion HShiftUpe; subst; try lia.
    inversion HSubste''; subst; try lia.
    eapply VarShiftDoOfUp; eauto.
  - inversion HShiftUpe; subst; try lia.
    + inversion HSubste''; subst; try lia.
      constructor; lia.
    + inversion HSubste''; subst; try lia.
      constructor; lia.
  - inversion HShiftUpe; subst.
    inversion HSubste''; subst.
    constructor.

    admit.
  - inversion HShiftUpe; subst.
    inversion HSubste''; subst.
    constructor.
    eapply IHHSubste1; eauto.
    eapply IHHSubste2; eauto.
  - inversion HShiftUpe; subst.
    inversion HSubste''; subst.
    constructor.
    eapply IHHSubste; eauto.
Qed.

Lemma BEtaShiftUp:
  forall e k e', BEta e k e' -> forall eup e'up, VarShiftUp e 0 eup -> VarShiftUp e' 0 e'up -> exists l, BEta (Lam eup) l (Lam e'up).
Proof.
  intros e k e' HBEta.
  induction HBEta.
  - intros eup e'up HShiftUpe HShiftUpe'.
    inversion H; subst.
    inversion HShiftUpe; subst.
    inversion H5; subst.
    assert (sub' = arg') by (eapply VarShiftUpUniq; eauto); subst;
      clear H8.
    destruct (VarShiftUpDecidable arg' 0) as [ arg'' HShiftUparg' ].
    destruct (VarSubstDecidable body'1 0 arg'') as [ body'1' HSubstbody'1 ].
    assert (HNFree0arg': NotFreeVar arg' 0) by (eapply VarShiftUpNotFree; eauto).
    assert (HNFree0arg'': NotFreeVar arg'' 0) by (eapply VarShiftUpNotFree; eauto).
    assert (HNFree1arg'': NotFreeVar arg'' 1) by admit.
    assert (HNFree1body'1: NotFreeVar body'1 1) by (eapply VarShiftUpNotFree; eauto).
    assert (HNFree0body'1': NotFreeVar body'1' 0) by (eapply VarSubstNotFree; eauto).
    assert (HNFree1body'1': NotFreeVar body'1' 1) by admit.
    assert (HNFree0body'0: NotFreeVar body'0 0) by (eapply VarSubstNotFree; eauto).
    assert (e'up = body'0) by (eapply VarShiftUpDoUniq; eauto); subst.
    exists KBeta.
    apply BELam.
    eapply BEBeta.
    econstructor.
    eapply HShiftUparg'.
    eapply HSubstbody'1.


Qed.

Lemma BEtasShiftUp:
  forall e n e', BEtas e n e' -> forall eup e'up, VarShiftUp e 0 eup -> VarShiftUp e' 0 e'up -> BEtas (Lam eup) n (Lam e'up).
Proof.
  intros e n e' HBEtas.
  induction HBEtas.
  - intros eup e'up HShiftUpe HShiftUpe'.
    assert (e'up = eup) by (now apply (VarShiftUpUniq e 0 e'up HShiftUpe' eup)); subst.
    econstructor.
  - intros eup e''up HShiftUpe HShiftUpe''.
    destruct (VarShiftUpDecidable e' 0) as [ e'up HShiftUpe' ].
    assert (HBEtasup: BEtas (Lam eup) n (Lam e'up)) by now apply IHHBEtas.
    eapply BEtaS.
    eapply HBEtasup.

Qed.

Lemma BEtaEqIdShiftUp:
  forall e, BEtaEq e (Lam (Var 0)) -> forall eup, VarShiftUp e 0 eup -> BEtaEq (Lam eup) (Lam (Lam (Var 0))).
Proof.
  intros e HBEEq.
  inversion HBEEq; subst.
  generalize m H0; clear HBEEq m H0.
  induction H.
  - intros m HBEtasid eup HShiftUp.
    destruct (IdNoBEta m e HBEtasid); subst.
    inversion HShiftUp; subst.
    inversion H0; subst; try lia.
    repeat econstructor.
  - intros

Lemma BEtasIdShiftUp:
  forall e n e', BEtas e n e' -> forall m eup, BEtas e' m (Lam (Var 0)) -> VarShiftUp e 0 eup -> BEtas (Lam eup) n (Lam (Lam (Var 0))).
Proof.
  intros e n e' HBEtas.
  inversion HBEtas; subst.
  - intros m eup HBEtas' HShiftUp.
    inversion HShiftUp; subst.
    inversion H0; subst; try lia.
    constructor.
  - generalize k H0;
      clear HBEtas k H0.
    induction H.
    + intros k HBEta eup HShiftUp.
      eapply BEtaIdShiftUp; eauto.
    + intros k' HBEta eup HShiftUp.

Abort.


  forall e n e', BEtas e n e' -> forall k eup, BEta e' k (Lam (Var 0)) -> VarShiftUp e 0 eup -> BEtas (Lam eup) (S n) (Lam (Lam (Var 0))).
Proof.
  intros e n e' HBEtas.
  induction HBEtas.
  - intros k eup HBEta HShiftUp.
    eapply BEtaIdShiftUp; eauto.
  - intros k' eup HBEta HShiftUp.
    eapply BEtaS.
    eapply IHHBEtas.
    admit.
    auto.

  forall e n, BEtas e n (Lam (Var 0)) -> forall e', VarShiftUp e 0 e' -> BEtas (Lam e') n (Lam (Lam (Var 0))).
Proof.
  intros e n HBEtas.
  inversion HBEtas; subst.
  - intros e' HShiftUp.
    inversion HShiftUp; subst.
    inversion H0; subst; try lia.
    apply BEta0.
  - induction H.
    + intros e' HShiftUp.
      assert (HProof: BEtas (Lam e') 1 (Lam (Lam (Var 0)))) by (eapply BEtaIdShiftUp; eauto).
      inversion HProof; subst.
      inversion H1; subst.
      eapply BEtaS.
      eapply BEta0.
      apply H3.
    + intros e''' HShiftUp.

  remember (Lam (Var 0)) as id;
    induction HBEtas;
    try discriminate; subst.
  - sauto lq: on.
  - inversion H; subst.
    +
