From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftDoDec:
  forall t threshold, UniShiftDoSafe t threshold -> exists t', UniShiftDo t threshold t'.
Proof.
  intros t threshold HShiftSafe.
  induction HShiftSafe.
  - exists TUnt; constructor.
  - destruct IHHShiftSafe1 as [ tl' HShiftDotl ].
    destruct IHHShiftSafe2 as [ tr' HShiftDotr ].
    exists (TFun tl' tr').
    now constructor.
  - exists (TUni 0).
    constructor; lia.
  - destruct (PeanoNat.Nat.lt_trichotomy (S v) threshold) as [ HLt | [ HEq | HGt ] ].
    + exists (TUni (S v)).
      constructor; auto.
    + subst.
      exists (TUni v).
      constructor; lia.
    + exists (TUni v).
      constructor; lia.
  - destruct IHHShiftSafe as [ t' HShiftDo ].
    exists (TAll t').
    now constructor.
  - exists (TExV v).
    constructor.
Qed.
