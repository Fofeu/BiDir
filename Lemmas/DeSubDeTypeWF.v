From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import
  UniMapInvDeTypeWF
  UniShiftDoOfUp
  DeCtxCutBUniDeTypeWF.

Lemma DeSubDeTypeWF:
  forall ctx tl tr,
    DeSub ctx tl tr -> (DeTypeWF ctx tl /\ DeTypeWF ctx tr).
Proof.
  intros ctx tl tr HSub.
  induction HSub.
  - repeat constructor.
  - destruct IHHSub1.
    destruct IHHSub2.
    repeat constructor; auto.
  - repeat constructor; auto.
  - destruct IHHSub as [ HTypeWFtabs HTypeWFtr ].
    split; auto.
    now apply (UniMapInvDeTypeWF ctx (TAll tabs) tau tabs').
  - destruct IHHSub.
    assert (HShiftDo: UniShiftDo tl' 0 tl) by (now apply UniShiftDoOfUp).
    split; repeat constructor; auto.
    apply (DeCtxCutBUniDeTypeWF (BUni :: ctx) _ H1 0); auto.
    constructor.
Qed.
