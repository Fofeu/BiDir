From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.
From BiDir Require Import SystemF.

From BiDir.Lemmas Require Import DeCtxInsertBVarDeCtxInVarPost.
From BiDir.Lemmas Require Import DeCtxInsertBVarDeCtxInVarPre.
From BiDir.Lemmas Require Import DeCtxInsertBVarDeTypeWF.

Lemma VarShiftUpSysFInf:
  forall ctx e t,
    SysFInf ctx e t -> forall n e' tb ctx', VarShiftUp e n e' -> DeCtxInsertBVar ctx n tb ctx' -> DeCtxWF ctx' -> SysFInf ctx' e' t.
Proof.
  intros ctx e t HSysF.
  induction HSysF;
    intros n e' tb ctx' HShiftUp HCtxInsert HCtxWF.
  - inversion HShiftUp; subst.
    now constructor.
  - inversion HShiftUp; subst; constructor; auto.
    + eapply DeCtxInsertBVarDeCtxInVarPost; now eauto.
    + eapply DeCtxInsertBVarDeCtxInVarPre; now eauto.
  - inversion HShiftUp; subst.
    constructor; auto.
    assert (HECtxWF: DeCtxWF (BVar tl :: ctx)) by now inversion HSysF.
    assert (HTypeWF: DeTypeWF ctx tl) by now inversion HECtxWF.
    apply (IHHSysF (S n) _ tb); auto.
    now repeat constructor.
    constructor; auto.
    eapply DeCtxInsertBVarDeTypeWF; eauto.
  - inversion HShiftUp; subst.
    apply (SysFApp _ _ _ tl); auto.
    now apply (IHHSysF1 n _ tb).
    now apply (IHHSysF2 n _ tb).
  - constructor; auto.
    apply (IHHSysF n _ tb); auto.
    now constructor.
    now constructor.
  - inversion H2; subst.
    apply (SysFAbE _ _ t tau); auto.
    apply (IHHSysF n _ tb); auto.
    eapply DeCtxInsertBVarDeTypeWF; eauto.
Qed.
