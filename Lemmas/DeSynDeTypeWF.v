From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import DeCtxInVarDeTypeWF.
From BiDir.Lemmas Require Import DeAppDeTypeWF.

Lemma DeSynDeTypeWF:
  forall ctx e t,
    DeSyn ctx e t -> DeTypeWF ctx t.
Proof.
  intros ctx e t HSyn.
  induction HSyn.
  - constructor.
  - now apply (DeCtxInVarDeTypeWF _ v).
  - auto.
  - apply (DeAppDeTypeWF ctx tf arg tapp); auto.
  - auto.
Qed.
