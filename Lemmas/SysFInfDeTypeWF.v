From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import SystemF.

From BiDir.Lemmas Require Import DeCtxInVarDeTypeWF.
From BiDir.Lemmas Require Import DeCtxCutBVarDeTypeWF.
From BiDir.Lemmas Require Import UniMapDeTypeWF.

Lemma SysFInfDeTypeWF:
  forall ctx e t,
    SysFInf ctx e t -> DeTypeWF ctx t.
Proof.
  intros ctx e t HSysF.
  induction HSysF.
  - constructor.
  - apply (DeCtxInVarDeTypeWF _ v); auto.
  - constructor.
    inversion HSysF; subst; inversion H0; subst; auto.
    eapply DeCtxCutBVarDeTypeWF; eauto.
    econstructor.
  - inversion IHHSysF1; auto.
  - constructor; auto.
  - now apply (UniMapTypeWF ctx (TAll t) tau).
Qed.
