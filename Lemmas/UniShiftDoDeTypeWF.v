From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import DeCtxCutBUniDeCtxMemUniGE.
From BiDir.Lemmas Require Import DeCtxCutBUniDeCtxMemUniLt.
From BiDir.Lemmas Require Import DeCtxCutBUniExtended.

Lemma UniShiftDoTypeWF:
  forall t n t', UniShiftDo t n t' -> forall ctx ctx', DeTypeWF ctx t -> DeCtxCutBUni ctx n ctx' -> NotFreeUni t n -> DeTypeWF ctx' t'.
Proof.
  intros t n t' HShiftDo.
  induction HShiftDo.
  - constructor.
  - intros ctx ctx' HTypeWF HCtxCut HNFree.
    inversion HTypeWF; subst.
    inversion HNFree; subst.
    constructor.
    now apply (IHHShiftDo1 ctx).
    now apply (IHHShiftDo2 ctx).
  - intros ctx ctx' HTypeWF HCtxCut HNFree.
    inversion HTypeWF; subst.
    inversion HNFree; subst.
    constructor.
    eapply (DeCtxCutBUniDeCtxMemUniGE); eauto; lia.
  - intros ctx ctx' HTypeWF HCtxCut HNFree.
    inversion HTypeWF; subst.
    constructor.
    eapply (DeCtxCutBUniDeCtxMemUniLt); eauto.
  - intros ctx ctx' HTypeWF HCtxCut HNFree.
    inversion HTypeWF; subst.
    constructor.
    apply (IHHShiftDo (BUni::ctx)); auto.
    + now apply DeCtxCutBUniExtended.
    + now inversion HNFree.
  - intros ctx ctx' HTypeWF HCtxCut HNFree.
    inversion HTypeWF.
Qed.
