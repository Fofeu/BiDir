From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftUpDec:
  forall t threshold,
  exists t', UniShiftUp t threshold t'.
Proof.
  intros t.
  induction t.
  - exists TUnt; constructor.
  - intros threshold.
    destruct (IHt1 threshold) as [t1' HShiftt1].
    destruct (IHt2 threshold) as [t2' HShiftt2].
    exists (TFun t1' t2').
    constructor; auto.
  - intros threshold.
    destruct (PeanoNat.Nat.lt_trichotomy threshold n) as [HLt | [HEq | HGt]].
    + exists (TUni (S n)).
      apply UniShUpTUnM; lia.
    + exists (TUni (S n)).
      apply UniShUpTUnM; lia.
    + exists (TUni n).
      apply UniShUpTUnN; auto.
  - intros n.
    destruct (IHt (S n)) as [t' HShiftUp].
    exists (TAll t').
    constructor; auto.
  - intros threshold.
    exists (TExV n).
    constructor.
Qed.
