From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma BEtasChainApp:
  forall f n f' , BEtas f n f' -> forall arg m arg', BEtas arg m arg' -> BEtas (App f arg) (n + m) (App f' arg').
Proof.
  intros f n f' HBEtas.
  induction HBEtas.
  - intros arg m arg' HBEtasarg.
    induction HBEtasarg.
    + constructor.
    + apply (BEtaS _ n (App e e') k); auto.
      now apply BEAppA.
  - intros arg m arg' HBEtasarg.
    apply (BEtaS _ (n + m) (App e' arg') k).
    apply IHHBEtas.
    apply HBEtasarg.
    now repeat constructor.
Qed.
