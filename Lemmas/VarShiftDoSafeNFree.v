From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma VarShiftDoSafeNFree:
  forall e v, NotFreeVar e v -> VarShiftDoSafe e v.
Proof.
  intros e v HNFree.
  induction HNFree.
  - constructor.
  - destruct n,v; try lia; constructor.
  - now constructor.
  - now constructor.
  - now constructor.
Qed.
