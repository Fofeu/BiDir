From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma DeCtxCutBUniExtended:
  forall ctx n ctx',
    DeCtxCutBUni ctx n ctx' -> DeCtxCutBUni (BUni :: ctx) (S n) (BUni :: ctx').
Proof.
  intros ctx n ctx' HCtxCut.
  induction HCtxCut.
  - repeat constructor.
  - repeat constructor.
    now inversion IHHCtxCut.
  - repeat constructor.
    now inversion IHHCtxCut.
Qed.
