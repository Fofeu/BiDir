From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxMemUniLt:
  forall ctx v, DeCtxMemUni ctx v -> forall v', v' < v -> DeCtxMemUni ctx v'.
Proof.
  intros ctx v HCtxMem.
  induction HCtxMem.
  - intros; lia.
  - intros v' HLt.
    constructor.
    apply IHHCtxMem; auto.
  - intros v' HLt.
    destruct v'.
    + constructor.
    + assert (HLt': v' < v) by lia.
      constructor.
      now apply IHHCtxMem.
Qed.
