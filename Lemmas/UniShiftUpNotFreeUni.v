From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftUpNotFreeUni:
  forall t n t', UniShiftUp t n t' -> NotFreeUni t' n.
Proof.
  intros t n t' HShiftUp.
  induction HShiftUp.
  - constructor.
  - constructor; auto.
  - constructor; lia.
  - constructor; lia.
  - constructor; auto.
  - constructor.
Qed.
