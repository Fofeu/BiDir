From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import DeCtxCutBVarDeCtxMemUni.

Lemma DeCtxCutBVarDeTypeWF:
  forall ctx t, DeTypeWF ctx t -> forall n ctx', DeCtxCutBVar ctx n ctx' -> DeTypeWF ctx' t.
Proof.
  intros ctx t HTypeWF.
  induction HTypeWF.
  - constructor.
  - intros n ctx' HCtxCut.
    constructor; eauto.
  - intros n ctx' HCtxCut.
    constructor.
    apply (DeCtxCutBVarDeCtxMemUni ctx v H n ctx' HCtxCut).
  - intros n ctx' HCtxCut.
    constructor.
    apply (IHHTypeWF n).
    now constructor.
Qed.
