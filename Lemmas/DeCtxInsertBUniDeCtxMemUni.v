From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import LE_cases.
From BiDir.Lemmas Require Import DeCtxMemUniLt.

From Coq Require Import Lia.

Lemma DeCtxInsertBUniDeCtxMemUni:
  forall ctx n ctx', DeCtxInsertBUni ctx n ctx' -> forall v v', DeCtxMemUni ctx v -> v' <= (S v) -> DeCtxMemUni ctx' v'.
Proof.
  intros ctx n ctx' HCtxInsert.
  induction HCtxInsert; intros v v' HCtxMem HLE.
  - destruct (LE_cases v' (S v) HLE) as [ HLt | Heq ]; subst.
    + destruct v, v'; try lia.
      constructor.
      constructor.
      constructor.
      inversion HCtxMem; subst;
        try constructor;
        apply (DeCtxMemUniLt _ (S v)); auto; lia.
    + now constructor.
  - inversion HCtxMem; subst.
    constructor.
    now apply (IHHCtxInsert v v').
  - destruct (LE_cases v' (S v) HLE) as [ HLt | Heq ]; subst; clear HLE.
    + destruct v, v'; try lia.
      * constructor.
      * constructor.
      * inversion HCtxMem; subst.
        constructor.
        apply (IHHCtxInsert v); auto; lia.
    + inversion HCtxMem; subst.
      * constructor.
        induction HCtxInsert.
        constructor.
        constructor; auto.
        apply IHHCtxInsert0.
        intros v v' HCtxMem' HLE.
        assert (HProof': DeCtxMemUni (BVar tv :: ctx) v) by now constructor.
        remember (IHHCtxInsert v v' HProof' HLE) as HProof.
        now inversion HProof.
        constructor.
        constructor.
      * constructor.
        apply (IHHCtxInsert v0); auto; lia.
Qed.
