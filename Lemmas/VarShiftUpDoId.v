From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma VarShiftUpDoId:
  forall t t' n,
    VarShiftUp t n t' -> forall t'', VarShiftDo t' n t'' -> t = t''.
Proof.
  intros t t' n HShiftUp.
  induction HShiftUp.
  - intros t'' HShiftDo; inversion HShiftDo; reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst; try lia; reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst; try lia; reflexivity.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    now apply IHHShiftUp.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    now apply IHHShiftUp1.
    now apply IHHShiftUp2.
  - intros t'' HShiftDo.
    inversion HShiftDo; subst.
    f_equal.
    now apply IHHShiftUp.
Qed.
