From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpDoId.

Lemma UniShiftUpDoTypeWF:
  forall ctx t t' t'' n,
    DeTypeWF ctx t -> UniShiftUp t n t' -> UniShiftDo t' n t'' -> DeTypeWF ctx t''.
Proof.
  intros ctx t t' t'' n HTypeWF HShUp HShDo.
  rewrite <- (UniShiftUpDoId t t' n HShUp t'' HShDo).
  auto.
Qed.
