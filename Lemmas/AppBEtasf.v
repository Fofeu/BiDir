From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma AppBEtasf:
  forall f n f', BEtas f n f' -> forall arg, BEtas (App f arg) n (App f' arg).
Proof.
  intros f n f' HBEtas.
  induction HBEtas.
  - constructor.
  - intros arg.
    eapply BEtaS.
    eapply (IHHBEtas arg).
    eapply BEAppF; eauto.
Qed.
