From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma UniMemPred:
  forall ctx v, DeCtxMem ctx (S v) -> DeCtxMem ctx v.
Proof.
  intros ctx.
  induction ctx.
  - intros v HCtxMem.
    inversion HCtxMem.
  - intros v HCtxMem.
    destruct a.
    + constructor.
      apply IHctx.
      inversion HCtxMem; subst; auto.
    + destruct v.
      constructor.
      constructor.
      apply IHctx.
      inversion HCtxMem; auto.
    + inversion HCtxMem.
    + inversion HCtxMem.
    + inversion HCtxMem.
Qed.
