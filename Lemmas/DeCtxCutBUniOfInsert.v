From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

Lemma DeCtxCutBUniOfInsert:
  forall ctx' n ctx, DeCtxInsertBUni ctx' n ctx -> DeCtxCutBUni ctx n ctx'.
Proof.
  intros ctx' n ctx HCtxInsert.
  induction HCtxInsert.
  - constructor.
  - now constructor.
  - now constructor.
Qed.
