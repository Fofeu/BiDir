From Coq Require Import Lia.

Lemma LE_cases:
  forall n m, n <= m -> n < m \/ n = m.
  intros n.
  induction n.
  - intros m HLE.
    inversion HLE; subst.
    constructor; reflexivity.
    constructor; lia.
  - intros m.
    induction m; intros HLE; try now inversion HLE.
    assert (HLE': n <= m) by now apply le_S_n.
    destruct (IHn m HLE') as [ HLt | HEq ].
    left; lia.
    right; lia.
Qed.
