From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma NoBVarCtxInFalse:
  forall ctx v t, DeCtxNoBVar ctx -> DeCtxIn ctx v t -> False.
Proof.
  intros ctx v t HNoBVar HCtxIn.
  induction HCtxIn.
  - inversion HNoBVar.
  - inversion HNoBVar.
  - apply IHHCtxIn.
    inversion HNoBVar; auto.
Qed.
