From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma BEtaChain:
  forall e k e' l e'', BEta e k e' -> BEta e' l e'' -> BEtas e 2 e''.
Proof.
  intros e k e' l e'' HBEtae HBEtae'.
  eapply BEtaS; try now apply HBEtae'.
  eapply BEtaS; try now apply HBEtae.
  apply BEta0.
Qed.
