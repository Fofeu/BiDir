From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftUpNotFree:
  forall e n e', VarShiftUp e n e' -> NotFreeVar e' n.
Proof.
  intros e n e' HShiftUp.
  induction HShiftUp.
  - constructor.
  - constructor; lia.
  - constructor; lia.
  - now constructor.
  - now constructor.
  - now constructor.
Qed.
