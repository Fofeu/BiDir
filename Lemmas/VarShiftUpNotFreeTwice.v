From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftUpNotFreeTwice:
  forall e m e', VarShiftUp e m e' -> forall n, NotFreeVar e n -> m < (S n) -> NotFreeVar e' (S n).
Proof.
  intros e m e' HShiftUp.
  induction HShiftUp.
  - constructor.
  - intros n HNFree HLt.
    constructor.
    inversion HNFree; subst; lia.
  - intros n HNFree HLt.
    constructor. inversion HNFree; subst.
    lia.
  - intros n HNFree HLt.
    constructor.
    apply IHHShiftUp.
    inversion HNFree; auto.
    lia.
  - intros n HNFree HLt.
    inversion HNFree; subst.
    constructor.
    now apply IHHShiftUp1.
    now apply IHHShiftUp2.
  - intros n HNFree HLt.
    inversion HNFree; subst.
    constructor.
    now apply IHHShiftUp.
Qed.
