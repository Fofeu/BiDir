From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpUniq.

From Coq Require Import Lia.

Lemma UniSubstUniq:
  forall t n sub t', UniSubst t n sub t' -> forall t'', UniSubst t n sub t'' -> t' = t''.
Proof.
  intros t n sub t' HSubst.
  induction HSubst.
  - intros t'' HSubst.
    inversion HSubst.
    reflexivity.
  - intros t'' HSubst.
    inversion HSubst; subst.
    f_equal.
    apply IHHSubst1; auto.
    apply IHHSubst2; auto.
  - intros t'' HSubst.
    inversion HSubst; subst; try lia; reflexivity.
  - intros t'' HSubst.
    inversion HSubst; subst; try lia; reflexivity.
  - intros t'' HSubst'.
    inversion HSubst'; subst.
    f_equal.
    rewrite (UniShiftUpUniq sub 0 sub' H sub'0) in *; auto.
  - intros t'' HSubst.
    inversion HSubst.
    reflexivity.
Qed.
