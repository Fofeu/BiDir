From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxInsertBVarDeCtxInVarPost:
  forall ctx n tn ctx', DeCtxInsertBVar ctx n tn ctx' -> forall v tv, DeCtxInVar ctx v tv -> n <= v -> DeCtxInVar ctx' (S v) tv.
Proof.
  intros ctx n tn ctx' HCtxInsert.
  induction HCtxInsert as
    [ ctx t
    | ctx n tn ctx' tv' HCtxInsert IHHCtxInsert
    | ctx n tn ctx' HCtxInsert IHHCtxInsert
    ];
    intros v tv HCtxIn HPost.
  - now constructor.
  - inversion HCtxIn; subst; try lia.
    constructor.
    apply IHHCtxInsert; auto; lia.
  - inversion HCtxIn; subst.
    econstructor; eauto.
Qed.
