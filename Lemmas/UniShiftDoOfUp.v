From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma UniShiftDoOfUp:
  forall e n e', UniShiftUp e n e' -> UniShiftDo e' n e.
Proof.
  intros e n e' HShiftUp.
  induction HShiftUp.
  - constructor.
  - now constructor.
  - constructor; lia.
  - now constructor.
  - now constructor.
  - constructor.
Qed.
