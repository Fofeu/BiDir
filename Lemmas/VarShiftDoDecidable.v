From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma VarShiftDoDecidable:
  forall e threshold, VarShiftDoSafe e threshold -> exists e', VarShiftDo e threshold e'.
Proof.
  intros e.
  induction e.
  - intros; exists Unt; now constructor.
  - intros threshold HSafeShift.
    inversion HSafeShift; subst.
    + exists (Var 0); constructor; lia.
    + destruct (PeanoNat.Nat.le_gt_cases threshold (S v)).
      * exists (Var v); now constructor.
      * exists (Var (S v)); now constructor.
  - intros threshold HSafeShift.
    inversion HSafeShift; subst.
    destruct (IHe (S threshold) H0) as [e' HShiftDo].
    exists (Lam e').
    now constructor.
  - intros threshold HSafeShift.
    inversion HSafeShift; subst.
    destruct (IHe1 threshold H1) as [ e1' HShiftDoe1 ].
    destruct (IHe2 threshold H3) as [ e2' HShiftDoe2 ].
    exists (App e1' e2').
    now constructor.
  - intros threshold HSafeShift.
    inversion HSafeShift; subst.
    destruct (IHe threshold H2) as [ e' HShiftDo ].
    exists (Ant e' t).
    auto.
    now constructor.
Qed.
