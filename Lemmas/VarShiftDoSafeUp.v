From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftDoSafeUp:
  forall e n e', VarShiftUp e n e' -> VarShiftDoSafe e' n.
Proof.
  intros e n e' HShiftUp.
  induction HShiftUp.
  - constructor.
  - constructor.
  - destruct threshold,v; try lia; constructor.
  - now constructor.
  - now constructor.
  - now constructor.
Qed.
