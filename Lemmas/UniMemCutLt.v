From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniMemCutLt:
  forall ctx n ctx', DeCtxCutBUni ctx n ctx' -> forall v, DeCtxMem ctx v -> v < n -> DeCtxMem ctx' v.
Proof.
  intros ctx n ctx' HCtxCut.
  induction HCtxCut.
  - intros; try lia.
  - intros v HCtxMem HLt.
    constructor.
    apply IHHCtxCut; auto.
    now inversion HCtxMem.
  - intros v HCtxMem HLt.
    destruct v.
    + constructor.
    + constructor.
      apply IHHCtxCut.
      now inversion HCtxMem.
      lia.
Qed.
