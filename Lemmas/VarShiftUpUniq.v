From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftUpUniq:
  forall t threshold t', VarShiftUp t threshold t' -> forall t'', VarShiftUp t threshold t'' -> t' = t''.
Proof.
  intros t threshold t' HShiftUp.
  induction HShiftUp.
  - intros t'' HShiftUp.
    inversion HShiftUp.
    reflexivity.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst; try lia.
    reflexivity.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst; try lia.
    reflexivity.
  - intros t'' HShiftUp'.
    inversion HShiftUp'; subst.
    f_equal.
    now apply IHHShiftUp.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst.
    f_equal.
    now apply IHHShiftUp1.
    now apply IHHShiftUp2.
  - intros t'' HShiftUp'.
    inversion HShiftUp'; subst.
    f_equal; try reflexivity.
    now apply IHHShiftUp.
Qed.
