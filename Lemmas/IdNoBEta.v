From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma IdNoBEta: forall n e, BEtas (Lam (Var 0)) n e -> n = 0 /\ e = (Lam (Var 0)).
Proof.
  intros n e HBEtas.
  remember (Lam (Var 0)) as lamvar0;
    induction HBEtas; try discriminate; subst.
  - constructor; reflexivity.
  - destruct (IHHBEtas eq_refl) as [ Heqn Heqe' ]; subst.
    inversion H; subst.
    inversion H1; subst.
Qed.
