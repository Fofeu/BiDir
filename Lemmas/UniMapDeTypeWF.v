From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftDoDeTypeWF.
From BiDir.Lemmas Require Import UniSubstDeTypeWF.
From BiDir.Lemmas Require Import UniShiftUpDeTypeWF.
From BiDir.Lemmas Require Import UniSubstNotFreeUni.
From BiDir.Lemmas Require Import UniShiftUpNotFreeUni.

Lemma UniMapTypeWF:
  forall ctx t sub t',
    DeTypeWF ctx t -> DeTypeWF ctx sub -> UniMap t sub t' -> DeTypeWF ctx t'.
Proof.
  intros ctx t sub t' HTypeWF HTypeWFsub HUniMap.
  destruct HUniMap as [ tabs sub tout sub' tabs' HShiftUp HSubst HShiftDo ].
  apply (UniShiftDoTypeWF tabs' 0 tout HShiftDo (BUni::ctx)).
  - apply (UniSubstTypeWF tabs 0 sub' tabs' HSubst (BUni :: ctx)).
    now inversion HTypeWF.
    apply (UniShiftUpDeTypeWF sub 0 sub' HShiftUp ctx); auto.
    repeat constructor.
  - constructor.
  - apply (UniSubstNotFreeUni tabs 0 sub'); auto.
    now apply (UniShiftUpNotFreeUni sub 0 sub').
Qed.
