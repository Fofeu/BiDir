From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma MonotypeDecidable:
  forall t,
    Decidable.decidable (Monotype t).
Proof.
  intros t.
  induction t.
  - repeat constructor.
  - destruct IHt1, IHt2.
    + repeat constructor; auto.
    + constructor; intros contra;
        inversion contra; contradiction.
    + constructor; intros contra;
        inversion contra; contradiction.
    + constructor; intros contra;
        inversion contra; contradiction.
  - repeat constructor.
  - right;
      intros contra;
      inversion contra.
  - repeat constructor.
Qed.
