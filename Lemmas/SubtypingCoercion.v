From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.
From BiDir Require Import SystemF.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.
From Hammer Require Import Tactics Hammer.

From BiDir.Lemmas Require Import
  VarShiftUpDecidable
  DeSubDeTypeWF
  VarShiftUpSysFInf
  IdNoBEta
  UniMapInvDeTypeWF
  VarShiftUpNotFree
  BEtasChain
  VarShiftDoOfUp
  DeCtxCutBVarSysFInf
  SysFCtxWeaken
  UniShiftUpNotFreeUni
  DeCtxCutBUniDeTypeWF
  DeCtxInsertBVarDeTypeWF.

Lemma SubtypingCoercion:
  forall ctx tl tr,
    DeSub ctx tl tr -> exists f, SysFInf ctx f (TFun tl tr) /\ BEtaEq f (Lam (Var 0)).
Proof.
  intros ctx tl tr HSub.
  induction HSub.
  - exists (Lam (Var 0)).
    split; try now constructor.
    repeat constructor; auto.
    repeat econstructor.
  - destruct IHHSub1 as [ g [ HSysFg HBEtaEqg ] ].
    destruct IHHSub2 as [ k [ HSysFk HBEtaEqk ] ].
    destruct (VarShiftUpDecidable g 0) as [ g' HShiftUpg ].
    destruct (VarShiftUpDecidable g' 0) as [ g'' HShiftUpg' ].
    destruct (VarShiftUpDecidable k 0) as [ k' HShiftUpk ].
    destruct (VarShiftUpDecidable k' 0) as [ k'' HShiftUpk' ].
    assert (HTypeWFh: DeTypeWF ctx (TFun tl1 tr1)) by (constructor; sfirstorder use: DeSubDeTypeWF).
    assert (HTypeWFx: DeTypeWF (BVar (TFun tl1 tr1) :: ctx) tl2) by
      (eapply DeCtxInsertBVarDeTypeWF; try econstructor; destruct (DeSubDeTypeWF ctx tl2 tl1); eauto).
    assert (HECtxWF: DeCtxWF (BVar (TFun tl1 tr1) :: ctx)) by
      (constructor; auto; constructor; sfirstorder use: DeSubTypeWF).
    assert (HEECtxWF: DeCtxWF (BVar tl2 :: BVar (TFun tl1 tr1) :: ctx)) by
      (constructor; auto; apply ExtensionBVarTypeWF; destruct (DeSubTypeWF ctx tl2 tl1); auto).
    assert (HSysFg': SysFInf (BVar (TFun tl1 tr1) :: ctx) g' (TFun tl2 tl1)) by
      (apply (VarShiftUpSysFInf ctx g _ HSysFg 0 _ (TFun tl1 tr1)); auto; constructor).
    assert (HSysFg'': SysFInf (BVar tl2 :: BVar (TFun tl1 tr1) :: ctx) g'' (TFun tl2 tl1)) by
      (apply (VarShiftUpSysFInf (BVar (TFun tl1 tr1) :: ctx) g' _ HSysFg' 0 _ tl2); auto; repeat constructor).
    assert (HSysFk': SysFInf (BVar (TFun tl1 tr1) :: ctx) k' (TFun tr1 tr2)) by
      (apply (VarShiftUpSysFInf ctx k _ HSysFk 0 _ (TFun tl1 tr1)); auto; constructor).
    assert (HSysFk'': SysFInf (BVar tl2 :: BVar (TFun tl1 tr1) :: ctx) k'' (TFun tr1 tr2)) by
      (apply (VarShiftUpSysFInf (BVar (TFun tl1 tr1) :: ctx) k' _ HSysFk' 0 _ tl2); auto; repeat constructor).
    exists (Lam (* h *) (Lam (* x *) (App k'' (App (Var 1) (App g'' (Var 0)))))).
    split.
    + apply SysFLam; auto.
      apply SysFLam; auto.
      apply (SysFApp _ _ _ tr1); auto.
      apply (SysFApp _ _ _ tl1); auto.
      { constructor; auto; repeat constructor. }
      apply (SysFApp _ _ _ tl2); auto.
      constructor; auto; repeat constructor.
    + inversion HBEtaEqg; inversion HBEtaEqk; subst.
      destruct (IdNoBEta m e H1); subst; destruct (IdNoBEta m0 e0 H5); subst.
      (* assert (HBEtasg'': BEtas g'' n (Lam (Var 0))) by admit. *)
      (* assert (HBEtask'': BEtas g'' n0 (Lam (Var 0))) by admit. *)
      apply (BEEq _ _ (Lam (Var 0)) (S (n + n0)) 0); try now constructor.
      (* Must first reduce g'' because it as an identity *)
      (* Then reduce k'' because it is an identity *)
      (* Then it should look like (Lam (Lam (Var 1) (Var 0))) which is eta-ruleable into (Lam (Var 0)) *)
      eapply BEtaS; (try (apply (BEEta (Lam (Var 0))); repeat constructor; lia)).
      apply (BEtasChain _ _ (Lam (Lam (App k'' (App (Var 1) (App (Lam (Var 0)) (Var 0))))))).
      * admit.
      * admit.
  - exists (Lam (Var 0)).
    now repeat econstructor.
  - destruct IHHSub as [ g [ HSysFg HBEtaEqg ] ].
    inversion HBEtaEqg; subst.
    destruct (IdNoBEta m e H4); subst.
    inversion H2; subst.
    destruct (VarShiftUpDecidable g 0) as [ g' HShiftUpg ].
    assert (HECtxWF: DeCtxWF (BVar (TAll tabs) :: ctx)) by (constructor; auto; apply (UniMapInvDeTypeWF _ _ tau tabs'); auto; sfirstorder use: DeSubDeTypeWF).
    exists (Lam (App g' (Var 0))).
    split.
    + apply SysFLam; auto.
      apply (SysFApp _ _ _ tabs').
      * constructor; auto.
        assert (HTypeWFtabs': DeTypeWF ctx tabs') by (sfirstorder use: DeSubDeTypeWF).
        now apply (UniMapInvDeTypeWF ctx (TAll tabs) tau tabs').
      * eapply VarShiftUpSysFInf; eauto; repeat constructor.
      * apply (SysFAbE _ _ tabs tau); auto.
        constructor; auto; repeat constructor.
        eapply DeCtxInsertBVarDeTypeWF; eauto.
        econstructor.
    + (* (Var 0) is non-free in g' thus we can eta-rule *)
      assert (HNFree: NotFreeVar g' 0) by (eapply VarShiftUpNotFree; eauto).
      eapply (BEEq _ _ (Lam (Var 0)) (1 + n) 0); auto.
      apply (BEtasChain _ _ g); auto.
      apply (BEtaS _ _ (Lam (App g' (Var 0))) KEta).
      apply BEta0.
      apply BEEta; auto.
      now apply VarShiftDoOfUp.
  - destruct IHHSub as [ g [ HSysFg HBEtaEqg ] ].
    inversion HBEtaEqg; subst.
    destruct (IdNoBEta m e H2); subst.
    destruct (VarShiftUpDecidable g 0) as [ g' HShiftUp ].
    assert (HTypeWFtl': DeTypeWF (BUni :: ctx) tl') by now destruct (DeSubDeTypeWF (BUni :: ctx) tl' tr).
    assert (HTypeWFtl: DeTypeWF ctx tl) by
      (eapply DeCtxCutBUniDeTypeWF; eauto; constructor).
    assert (HECtxWF: DeCtxWF (BVar tl :: ctx)) by (now constructor).
    assert (HEECtxWF: DeCtxWF (BUni :: BVar tl :: ctx)) by (now constructor).
    assert (HNFreetl': NotFreeUni tl' 0) by (eapply UniShiftUpNotFreeUni; eauto).
    assert (HNFree: NotFreeVar g' 0) by (eapply VarShiftUpNotFree; eauto).
    exists (Lam (App g' (Var 0))).
    split.
    + apply SysFLam; auto.
      apply SysFAbI; auto.
      apply (SysFApp _ _ _ tl').
      * now constructor.
      * apply (SysFCtxWeakenBVar _ _ _ HSysFg 0 tl);
          repeat constructor; auto.
      * constructor; auto.
        repeat econstructor; eauto.
    + apply (BEEq _ _ (Lam (Var 0)) (1 + n) 0); auto.
      apply (BEtasChain _ _ g); auto.
      eapply BEtaS.
      eapply BEta0.
      apply BEEta; auto.
      now apply VarShiftDoOfUp.
Admitted.
