From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import VarShiftUpDecidable.
From BiDir.Lemmas Require Import VarShiftDoDecidable.
From BiDir.Lemmas Require Import VarSubstDecidable.
From BiDir.Lemmas Require Import VarShiftUpNotFree.
From BiDir.Lemmas Require Import VarSubstNotFree.
From BiDir.Lemmas Require Import VarShiftDoSafeNFree.

Lemma VarMapDecidable:
  forall e sub, exists e', VarMap (Lam e) sub e'.
Proof.
  intros e sub.
  destruct (VarShiftUpDecidable sub 0) as [ sub' HShiftUp ].
  destruct (VarSubstDecidable e 0 sub') as [ e'' HSubst ].
  assert (HNFreesub': NotFreeVar sub' 0) by (eapply VarShiftUpNotFree; eauto).
  assert (HNFreee'': NotFreeVar e'' 0) by (eapply VarSubstNotFree; eauto).
  assert (HShiftDoSafe: VarShiftDoSafe e'' 0) by now apply VarShiftDoSafeNFree.
  destruct (VarShiftDoDecidable e'' 0 HShiftDoSafe) as [ e' HShiftDo ].
  exists e'.
  repeat econstructor; eauto.
Qed.
