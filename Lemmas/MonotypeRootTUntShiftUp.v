From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

Lemma MonotypeRootTUntShiftUp:
  forall t,
    MonotypeRoot t TUnt -> forall n, UniShiftUp t n t.
Proof.
  intros t HMRoot.
  remember TUnt as tunt.
  induction HMRoot; try discriminate.
  - subst; constructor.
  - subst.
    constructor.
    apply (IHHMRoot eq_refl).
Qed.
