From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

From BiDir.Lemmas Require Import
  UniShiftUpDec
  UniSubstDec
  UniShiftUpNotFreeUni
  UniSubstNotFreeUni.

Local Lemma L:
  forall t' n t'', UniSubst t' n (TUni (S n)) t'' -> forall t, UniShiftUp t (S n) t' -> UniShiftDo t'' n t.
Proof.
  intros t' n t'' HSubst.
  remember (TUni (S n)) as sub;
    induction HSubst; subst.
  - intros t HShiftUp.
    inversion HShiftUp; subst.
    constructor.
  - intros t HShiftUp.
    inversion HShiftUp; subst.
    constructor.
    now apply (IHHSubst1 eq_refl).
    now apply (IHHSubst2 eq_refl).
  - intros t HShiftUp.
    inversion HShiftUp; subst.
    + lia.
    + apply UniShDoTUnM.
      lia.
  - intros t HShiftUp.
    inversion HShiftUp; subst.
    + constructor; auto; lia.
    + constructor; auto; lia.
  - inversion H; subst; try lia.
    intros t'' HShiftUp.
    inversion HShiftUp; subst.
    constructor.
    now apply (IHHSubst eq_refl).
  - intros t HShiftUp.
    inversion HShiftUp; subst.
    constructor.
Qed.

Lemma SubRefl:
  forall t ctx,
    DeCtxWF ctx -> DeTypeWF ctx t -> DeSub ctx t t.
Proof.
  induction t.
  - now constructor.
  - intros ctx HCtxWF HTypeWF.
    inversion HTypeWF; subst.
    constructor; auto.
  - intros ctx HCtxWF HTypeWF.
    inversion HTypeWF; subst.
    now constructor.
  - intros ctx HCtxWF HTypeWF.
    assert (HECtxWF: DeCtxWF (BUni :: ctx)) by now constructor.
    inversion HTypeWF; subst.
    destruct (UniShiftUpDec (TAll t) 0) as [ tl' HShiftUp ].
    inversion HShiftUp; subst.
    apply (DeSubTAlR ctx (TAll t) t (TAll tabs')); auto.
    apply (DeSubTAlL (BUni :: ctx) tabs' t (TUni 0) t).
    + auto.
    + repeat constructor.
    + constructor.
    + destruct (UniSubstDec tabs' 0 (TUni 1)) as [ tabs'' HSubst ].
      assert (HNFree1: NotFreeUni tabs' 1) by (eapply UniShiftUpNotFreeUni; eauto).
      assert (HNFree0: NotFreeUni tabs'' 0) by (eapply UniSubstNotFreeUni; eauto; constructor; lia).
      eapply (UniMapBase tabs' (TUni 0) t (TUni 1) _).
      * repeat constructor.
      * apply HSubst.
      * eapply L; eauto.
    + apply IHt; auto.
  - intros ctx HCtxWF HTypeWF.
    inversion HTypeWF.
Qed.
