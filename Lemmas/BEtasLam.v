From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma BEtasLam:
  forall e n e', BEtas e n e' -> BEtas (Lam e) n (Lam e').
Proof.
  intros e n e' HBEtas.
  induction HBEtas.
  - constructor.
  - apply (BEtaS (Lam e) n (Lam e') k (Lam e'')).
    apply IHHBEtas.
    apply BELam.
    apply H.
Qed.
