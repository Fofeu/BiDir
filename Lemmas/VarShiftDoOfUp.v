From BiDir Require Import Lang.

From Coq Require Import Lia.

Lemma VarShiftDoOfUp:
  forall e n e', VarShiftUp e n e' -> VarShiftDo e' n e.
Proof.
  intros e n e' HShiftUp.
  induction HShiftUp.
  - constructor.
  - constructor; lia.
  - now constructor.
  - now constructor.
  - now constructor.
  - now constructor.
Qed.
