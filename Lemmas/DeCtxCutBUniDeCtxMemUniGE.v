From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxCutBUniDeCtxMemUniGE:
  forall ctx n ctx', DeCtxCutBUni ctx n ctx' -> forall v, DeCtxMemUni ctx (S v) -> n <= v -> DeCtxMemUni ctx' v.
Proof.
  intros ctx n ctx' HCtxCut.
  induction HCtxCut;
    intros v HCtxMem HGE.
  - now inversion HCtxMem.
  - inversion HCtxMem; subst.
    constructor.
    apply IHHCtxCut; auto; lia.
  - inversion HCtxMem; subst.
    destruct v; try now constructor.
    constructor.
    apply IHHCtxCut; auto; lia.
Qed.
