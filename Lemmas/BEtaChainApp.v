From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

Lemma BEtaChainApp:
  forall f k f' arg l arg', BEta f k f' -> BEta arg l arg' -> BEtas (App f arg) 2 (App f' arg').
Proof.
  intros f k f' arg l arg' HBEtaf HBEtaarg.
  apply (BEtaS _ 1 (App f' arg) l).
  apply (BEtaS _ 0 (App f arg) k).
  constructor.
  now apply BEAppF.
  now apply BEAppA.
Qed.
