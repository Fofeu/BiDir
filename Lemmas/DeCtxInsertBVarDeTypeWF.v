From BiDir Require Import Lang.
From BiDir Require Import DeCtx.

From BiDir.Lemmas Require Import DeCtxInsertBVarDeCtxMemUni.
From BiDir.Lemmas Require Import DeCtxInsertBVarBUniExtended.

Lemma DeCtxInsertBVarDeTypeWF:
  forall ctx t, DeTypeWF ctx t -> forall n tn ctx', DeCtxInsertBVar ctx n tn ctx' -> DeTypeWF ctx' t.
Proof.
  intros ctx t HTypeWF.
  induction HTypeWF;
    intros n tn ctx' HCtxInsert.
  - constructor.
  - constructor.
    now apply (IHHTypeWF1 n tn).
    now apply (IHHTypeWF2 n tn).
  - constructor.
    now apply (DeCtxInsertBVarCtxMem ctx n tn ctx').
  - constructor.
    apply (IHHTypeWF n tn).
    now apply DeCtxInsertBVarBUniExtended.
Qed.
