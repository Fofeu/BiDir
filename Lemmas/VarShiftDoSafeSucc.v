From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma VarShiftDoSafeSucc:
  forall e n, VarShiftDoSafe e (S n).
Proof.
  induction e.
  - constructor.
  - destruct n; now constructor.
  - constructor; auto.
  - constructor; auto.
  - constructor; auto.
Qed.
