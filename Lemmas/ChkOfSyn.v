From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import SubRefl.
From BiDir.Lemmas Require Import DeSynDeTypeWF.

Lemma ChkOfSyn:
  forall ctx e t,
    DeSyn ctx e t -> DeChk ctx e t.
Proof.
  intros ctx e t HSyn.
  assert (HCtxWF: DeCtxWF ctx) by now inversion HSyn.
  apply (DeChkSub ctx e t t); auto.
  apply SubRefl; auto.
  apply (DeSynDeTypeWF ctx e t); auto.
Qed.
