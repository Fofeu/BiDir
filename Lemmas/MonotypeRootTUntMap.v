From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpDec.
From BiDir.Lemmas Require Import MonotypeRootTUntSubst.
From BiDir.Lemmas Require Import MonotypeRootTUntShiftDo.

Lemma MonotypeRootTUntMap:
  forall t,
    MonotypeRoot t TUnt -> forall sub, UniMap (TAll t) sub t.
Proof.
  intros t HMRoot.
  remember TUnt as tunt.
  induction HMRoot; try discriminate; subst.
  - intros sub.
    destruct (UniShiftUpDec sub 0) as [ sub' HShiftUp ].
    econstructor; eauto; econstructor.
  - intros sub.
    destruct (UniShiftUpDec sub 0) as [ sub' HShiftUp ].
    econstructor; eauto.
    apply MonotypeRootTUntSubst; constructor; auto.
    apply MonotypeRootTUntShiftDo; constructor; auto.
Qed.
