From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From BiDir.Lemmas Require Import AppBEtasf.
From BiDir.Lemmas Require Import VarShiftUpDecidable.
From BiDir.Lemmas Require Import VarShiftDoDecidable.
From BiDir.Lemmas Require Import VarShiftDoSafeUp.
From BiDir.Lemmas Require Import VarShiftUpDoId.

Lemma AppIdOfBEtas:
  forall id n e, BEtas id n (Lam (Var 0)) -> BEtas (App id e) (S n) e.
Proof.
  intros id n e HBEtas.
  apply (BEtaS _ n (App (Lam (Var 0)) e) KBeta).
  - now apply AppBEtasf.
  - destruct (VarShiftUpDecidable e 0) as [ e' HShiftUp ].
    assert (HShiftDoSafe: VarShiftDoSafe e' 0) by (eapply VarShiftDoSafeUp; eauto).
    destruct (VarShiftDoDecidable e' 0 HShiftDoSafe) as [ e'' HShiftDo ].
    destruct (VarShiftUpDoId e e' 0 HShiftUp e'' HShiftDo).
    apply BEBeta.
    eapply (VarMapBase _ _ _ e' e').
    + auto.
    + constructor.
    + auto.
Qed.
