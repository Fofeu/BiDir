From BiDir Require Import DeCtx.

From Coq Require Import Lia.

Lemma DeCtxCutBVarDeCtxInVarGE:
  forall ctx n ctx', DeCtxCutBVar ctx n ctx' -> forall v t, DeCtxInVar ctx (S v) t -> n <= v -> DeCtxInVar ctx' v t.
Proof.
  intros ctx n ctx' HCtxCut.
  induction HCtxCut; intros v t HCtxIn HGE.
  - now inversion HCtxIn.
  - destruct v; try lia.
    constructor.
    apply IHHCtxCut.
    now inversion HCtxIn.
    lia.
  - inversion HCtxIn; subst.
    econstructor; now eauto.
Qed.
