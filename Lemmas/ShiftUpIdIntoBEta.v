From BiDir Require Import Lang.
From BiDir Require Import BetaEta.

From Coq Require Import Lia.

Lemma ShiftUpIdIntoBEta:
  forall e, VarShiftUp (Lam (Var 0)) 0 e -> BEta (Lam (App e (Var 0))) KEta (Lam (Var 0)).
  intros e HShiftUp.
  inversion HShiftUp; inversion H0; subst; try lia.
  apply BEEta; repeat constructor; lia.
Qed.
