From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From BiDir.Lemmas Require Import UniShiftUpUniq.
From BiDir.Lemmas Require Import UniSubstUniq.
From BiDir.Lemmas Require Import UniShiftDoUniq.

Lemma UniMapUniq:
  forall t sub t', UniMap (TAll t) sub t' -> forall t'', UniMap (TAll t) sub t'' -> t' = t''.
Proof.
  intros t sub t' HMap t'' HMap'.
  inversion HMap; subst;
    rename H0 into HShiftUp; rename H1 into HSubst; rename H2 into HShiftDo.
  inversion HMap'; subst;
    rename sub'0 into sub''; rename tabs'0 into tabs'';
    rename H0 into HShiftUp'; rename H1 into HSubst'; rename H2 into HShiftDo'.
  assert (Heqsub': sub' = sub'') by (apply (UniShiftUpUniq sub 0 sub' HShiftUp sub'' HShiftUp'));
    subst.
  assert (Heqtabs': tabs' = tabs'') by (apply (UniSubstUniq t 0 sub'' tabs' HSubst tabs'' HSubst'));
    subst.
  apply (UniShiftDoUniq tabs'' 0 t' HShiftDo t'' HShiftDo').
Qed.
