From BiDir Require Import Lang.
From BiDir Require Import DeCtx.
From BiDir Require Import DeTyping.

From Coq Require Import Lia.

Lemma UniShiftUpUniq:
  forall t threshold t', UniShiftUp t threshold t' -> forall t'', UniShiftUp t threshold t'' -> t' = t''.
Proof.
  intros t threshold t' HShiftUp.
  induction HShiftUp.
  - intros t'' HShiftUp.
    inversion HShiftUp.
    reflexivity.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst.
    f_equal.
    apply IHHShiftUp1; auto.
    apply IHHShiftUp2; auto.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst; try lia.
    reflexivity.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst; try lia.
    reflexivity.
  - intros t'' HShiftUp'.
    inversion HShiftUp'; subst.
    f_equal.
    apply IHHShiftUp; auto.
  - intros t'' HShiftUp.
    inversion HShiftUp; subst.
    reflexivity.
Qed.
